// <copyright file="AppConstants.cs" company="Scandic Hotels">
// Copyright � Scandic
// </copyright>

namespace Scandic.Services.ServiceAgents
{
    #region References
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Text;
    using Scandic.Services.Framework;
    #endregion

    #region Enum
    /// <summary>
    /// Deeplink Redirect Pages
    /// </summary>
    public enum DeepLinkRedirectPage
    {
        /// <summary>
        /// DeepLink for SelectRate Page with Regular booking
        /// </summary>
        SelectRateRegular,

        /// <summary>
        /// DeepLink for SelectRate Page with Block booking
        /// </summary>
        SelectRateBlockCode,

        /// <summary>
        /// DeepLink for SelectRate Page with Promo booking
        /// </summary>
        SelectRatePromoCode,

        /// <summary>
        /// DeepLink for SelectHotel Page
        /// </summary>
        SelectHotel,

        /// <summary>
        /// DeepLink for HotelLandingPage 
        /// </summary>
        HotelLandingPage,

        /// <summary>
        /// DeepLink for HotelOverviewPage 
        /// </summary>
        FindHotelPage,

        /// <summary>
        /// DeepLink for Scanweb 
        /// </summary>
        HomePage,
    }

    /// <summary>
    /// Availability Apis enum
    /// </summary>
    public enum AvailabilityApis
    {
        /// <summary>
        /// Availability For City
        /// </summary>
        AvailabilityForCity,

        /// <summary>
        /// Availability For Certain Hotels
        /// </summary>
        AvailabilityForCertainHotels,

        /// <summary>
        /// Lowest Rate For City
        /// </summary>
        LowestRateForCity,
    }

    /// <summary>
    /// API Service Visibility
    /// </summary>
    public enum APIServiceVisibility
    {
        /// <summary>
        /// Implies that the service is visible.
        /// </summary>
        Visible = 1, 

        /// <summary>
        /// Implies that the service is not visible.
        /// </summary>
        NotVisible = 0
    }
    #endregion

    /// <summary>
    /// Class App Constants
    /// </summary>
    public class AppConstants
    {
        /// <summary>
        /// System Error
        /// </summary>
        public const string SYSTEMERROR = "SYSTEM ERROR";

        /// <summary>
        /// OWSEXCEPTION string
        /// </summary>
        public const string OWSEXCEPTION = "OWS EXCEPTION";

        /// <summary>
        /// GENERALEXCEPTION key and value
        /// </summary>
        public const string GENERALEXCEPTION = "GENERAL EXCEPTION";

        /// <summary>
        /// BLOCKCODEQUALIFYINGTYPE key and value
        /// </summary>
        public const string BLOCKCODEQUALIFYINGTYPE = "BLOCKCODE_BOOKING";

        /// <summary>
        /// Constant for Miscellaneous
        /// </summary>
        public const string MISC = "Miscellaneous";

        
   }

    public static class PaymentTypes
    {
        /// <summary>
        /// Constant for Prepaid
        /// </summary>
        public static string Prepaid = ConfigHelper.GetValue(ConfigKeys.PrepaidPayment);

        /// <summary>
        /// Constant for Postpaid
        /// </summary>
        public static string Postpaid = ConfigHelper.GetValue(ConfigKeys.PostpaidPayment);
    }

    /// <summary>
    /// Class OWS Exception Constants
    /// </summary>
    public class OwsExceptionConstants
    {
        /// <summary>
        /// PROMOTIONCODEINVALID key and value
        /// </summary>
        public const string PROMOTIONCODEINVALID = "41";

        /// <summary>
        /// PROPERTYNOTAVAILABLE key and value
        /// </summary>
        public const string PROPERTYNOTAVAILABLE = "10";

        /// <summary>
        /// INVALIDPROPERTY key and value
        /// </summary>
        public const string INVALIDPROPERTY = "06";

        /// <summary>
        /// PROPERTYRESTRICTED key and value
        /// </summary>
        public const string PROPERTYRESTRICTED = "25";

        /// <summary>
        /// SEECONTACTDETAILSONCHAININFO key and value
        /// </summary>
        public const string SEECONTACTDETAILSONCHAININFO = "15";
    }

    /// <summary>
    /// Class CMS Exception Constants
    /// </summary>
    public class CMSExceptionConstants
    {
        /// <summary>
        /// BLOCKNOTCONFIGURED key and value
        /// </summary>
        public const string BLOCKNOTCONFIGURED = "BLOCKNAMENOTFOUND001";
    }

    /// <summary>
    /// Transaction Type Code
    /// </summary>
    public class TransactionTypeCode
    {
        /// <summary>
        /// For Redemption or Award Stay
        /// </summary>
        public const string AW = "AW";

        /// <summary>
        ///  For Award Cancellation
        /// </summary>      
        public const string AWC = "AWC";

       /// <summary>
        /// For Normal Stay - Type1 Manual Adjustment
       /// </summary>
        public const string ST = "ST";
      
      /// <summary>
      /// For Other type Stay - Type2 Manual Adjustment
      /// </summary>
        public const string OT = "OT";
    }

    /// <summary>
    /// Class contains Opera Constants
    /// </summary>
    public class OwsMonitorConstants
    {
        /// <summary>
        /// ROOM PER NIGHT
        /// </summary>
        public const int ROOMPERNIGHT = 1;

        /// <summary>
        /// AVAILABILITY PER ROOM
        /// </summary>
        public const int PERROOM = 1;
        
        /// <summary>
        /// CHILDREN PER ROOM
        /// </summary>
        public const int CHILDRENPERROOM = 0;

        /// <summary>
        /// ADULTS PER ROOM
        /// </summary>
        public const int ADULTSPERROOM = 0;
     }
}
