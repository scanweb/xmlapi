﻿// <copyright file="HotelNetwork.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.BusinessEntity
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Entire meta of the hotel network
    /// </summary>
    [Serializable]
    public sealed class HotelNetwork
    {
        /// <summary>
        /// Initializes a new instance of the HotelNetwork class
        /// </summary>
        public HotelNetwork()
        {
            this.Continents = new List<Continent>();
        }

        /// <summary>
        /// Gets or sets the language in which data is to be filled (default: English)
        /// </summary>
        public string Language { get; set; }

        /// <summary>
        /// Gets or sets the property provider name (default: Scandic)
        /// </summary>
        public string Provider { get; set; }

        /// <summary>
        /// Gets the list of continents where provider operates
        /// </summary>
        public IList<Continent> Continents { get; private set; }

        #region Master Lists / Properties
        /// <summary>
        /// Gets a list of all countries
        /// </summary>
        public IEnumerable<Country> AllCountries
        {
            get
            {
                return this.Continents
                    .SelectMany(continent => continent.Countries);
            }
        }

        /// <summary>
        /// Gets a list of all cities
        /// </summary>
        public IEnumerable<City> AllCities
        {
            get
            {
                return this.AllCountries
                    .SelectMany(ctry => ctry.Cities);
            }
        }

        /// <summary>
        /// Gets a list of all hotels
        /// </summary>
        public IEnumerable<Hotel> AllHotels
        {
            get
            {
                return this.AllCities
                    .SelectMany(hotel => hotel.Hotels);
            }
        }

        /// <summary>
        /// Gets a list of all bookable hotels
        /// </summary>
        public IEnumerable<Hotel> BookableHotels
        {
            get
            {
                return this.AllHotels
                    .Where(hotel => hotel.IsBookable == true);
            }
        }
        #endregion

        #region Filtered Lists
        /// <summary>
        /// Gets a list of all cities in a country
        /// </summary>
        /// <param name="country">Country to filter by</param>
        /// <returns>List of all cities in a country</returns>
        public IEnumerable<City> CitiesInCountry(string country)
        {
            return this.AllCountries
                .Where(ctry => ctry.Id == country)
                .SelectMany(cty => cty.Cities);
        }

        /// <summary>
        /// Gets a list of all hotels in a city
        /// </summary>
        /// <param name="city">City to filter by</param>
        /// <returns>List of all hotels in a city</returns>
        public IEnumerable<Hotel> HotelsInCity(string city)
        {
            return this.AllCities
                .Where(cty => cty.Id.Equals(city, StringComparison.OrdinalIgnoreCase))
                .SelectMany(hotel => hotel.Hotels);
        }

        /// <summary>
        /// Gets a list of all bookable hotels in a city
        /// </summary>
        /// <param name="city">City to filter by</param>
        /// <returns>List of all bookable hotels in a city</returns>
        public IEnumerable<Hotel> BookableHotelsInCity(string city)
        {
            return this.HotelsInCity(city)
                .Where(hotel => hotel.IsBookable == true);
        }
        #endregion

        #region Is Valid
        /// <summary>
        /// Checks if the continent is valid
        /// </summary>
        /// <param name="continent">ID of the Continent to be checked</param>
        /// <returns>True if valid</returns>
        public bool IsContinentValid(string continent)
        {
            return this.Continents.Where(ctry => ctry.Id == continent).Count() > 0;
        }

        /// <summary>
        /// Checks if the Country is valid
        /// </summary>
        /// <param name="country">ID of the Country to be checked</param>
        /// <returns>True if valid</returns>
        public bool IsCountryValid(string country)
        {
            return this.AllCountries.Where(ctry => ctry.Id == country).Count() > 0;
        }

        /// <summary>
        /// Checks if the City is valid
        /// </summary>
        /// <param name="city">ID of the City to be checked</param>
        /// <returns>True if valid</returns>
        public bool IsCityValid(string city)
        {
            return this.AllCities
                .Where(cty => cty.Id.Equals(city, StringComparison.OrdinalIgnoreCase))
                .Count() > 0;
        }

        /// <summary>
        /// Checks if the Hotel is valid
        /// </summary>
        /// <param name="hotel">ID of the Hotel to be checked</param>
        /// <returns>True if valid</returns>
        public bool IsHotelValid(string hotel)
        {
            return this.AllHotels.Where(htl => htl.Id == hotel).Count() > 0;
        }

        /// <summary>
        /// Checks if the Hotel is valid and bookable
        /// </summary>
        /// <param name="hotel">ID of the Hotel to be checked</param>
        /// <returns>True if valid</returns>
        public bool IsHotelValidAndBookable(string hotel)
        {
            return this.BookableHotels.Where(htl => htl.Id == hotel).Count() > 0;
        }
        #endregion

        #region Lookups
        /// <summary>
        /// GetContinentForCountry method
        /// </summary>
        /// <param name="country">country value</param>
        /// <returns>continent for country</returns>
        public string GetContinentForCountry(string country)
        {
            Country selectedCountry = this.AllCountries.ToList<Country>().Find(delegate(Country cn)
            {
                return cn.Id.Equals(country);
            });

            if (selectedCountry != null)
            {
                return selectedCountry.Continent;
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// GetContinentForCity method
        /// </summary>
        /// <param name="city">city value</param>
        /// <returns>continent for city</returns>
        public string GetContinentForCity(string city)
        {
            //return this.Continents
            //    .Where(ct => ct.Countries.SelectMany(cty => cty.Cities).Select(cit => cit.Id == city).Count() > 0)
            //    .Select(cnt => cnt.Name).First();

            var continent = (from cnt in this.Continents
                             from country in cnt.Countries
                             from sity in country.Cities
                             where string.Equals(sity.Id, city, StringComparison.InvariantCultureIgnoreCase)
                             select cnt).FirstOrDefault();

            return continent != null ? continent.Name : "Europe";
        }

        /// <summary>
        /// return continent for a hotel
        /// </summary>
        /// <param name="hotelId">ID of the hotel</param>
        /// <returns>
        /// Name of Continent in which the hotel is
        /// </returns>
        public string GetContinentForHotel(string hotelId)
        {
            var continent = (from cnt in this.Continents
                             from country in cnt.Countries
                             from city in country.Cities
                             from hotel in city.Hotels
                             where string.Equals(hotel.Id, hotelId, StringComparison.InvariantCultureIgnoreCase)
                             select cnt).FirstOrDefault();

            return continent != null ? continent.Name : "Europe";
        }

        /// <summary>
        /// Gets the country code for hotel.
        /// </summary>
        /// <param name="hotelId">The hotel id.</param>
        /// <returns>Country Code in which the hotel is</returns>
        public string GetCountryCodeForHotel(string hotelId)
        {
            return this.AllCountries
               .Where(cn => cn.Cities.SelectMany(hts => hts.Hotels).Select(ht => ht.Id == hotelId).Count() > 0)
               .Select(cnt => cnt.Id).First();
        }


        public double GetVatForHotelFromCountry(string hotelId)
        {
            List<Country> listCountry = AllCountries.ToList();
            double strTax = 0;
            foreach (var item in listCountry)
            {
                foreach (var city in item.Cities)
                {
                    foreach (var hotel in city.Hotels)
                    {
                        if (hotel.Id == hotelId)
                        {
                            strTax = item.APITaxRates;
                            break;
                        }
                    }
                }
            }

            return strTax;
            //return this.AllCountries
            //   .Where(cn => cn.Cities.SelectMany(hts => hts.Hotels).Select(ht => ht.Id == hotelId).Count() > 0)
            //   .Select(cnt => cnt.APITaxRates).FirstOrDefault();
        }


        #endregion
    }
}
