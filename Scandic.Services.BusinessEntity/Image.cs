﻿// <copyright file="Image.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.BusinessEntity
{
    using System;
    using Scandic.Services.Framework;

    /// <summary>
    /// This class has the fields to hold the image of the hotel
    /// </summary>
    [Serializable]
    public class Image
    {
        /// <summary>
        /// Private variable holding the image URL
        /// </summary>
        private string imageUrl;

        /// <summary>
        /// Host URL for the image
        /// </summary>
        private string imageBase = ConfigHelper.GetValue(ConfigKeys.ImageBaseUri);

        /// <summary>           
        /// Gets or sets the ImageURL 
        /// </summary>  
        public string ImageUrl
        {
            get
            {
                return this.imageUrl;
            }

            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    this.imageUrl = value;
                }
                else
                {
                    string[] pieces = value.Split('~');
                    this.imageUrl = pieces.Length > 1 ? 
                        string.Format(
                        ConfigHelper.GetValue(ConfigKeys.ImageUrlTemplate),
                        pieces[1]) : 
                        this.imageBase + value;
                }
            }
        }

        /// <summary>           
        /// Gets or sets the Title
        /// </summary>  
        public string Title { get; set; }

        /// <summary>           
        /// Gets or sets whether this is the main Image
        /// </summary>  
        public string IsMainImage { get; set; }
    }
}
