﻿// <copyright file="RoomImage.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.BusinessEntity
{
    using System;   
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    using Scandic.Services.Framework;

    /// <summary>
    /// This class has the fields to hold the Room Image information
    /// </summary>
    [Serializable]
    [DataContract(
  Name = "RoomImage",
  Namespace = @"http://api.scandichotels.com/schemas")]
  public class RoomImage
    {
        /// <summary>
        /// Host URL for the image
        /// </summary>
        private static string imageUrlTemplate = ConfigHelper.GetValue(ConfigKeys.ImageUrlTemplate);     

        /// <summary>
        /// Host URL for the image
        /// </summary>
        private static string imageBase = ConfigHelper.GetValue(ConfigKeys.ImageBaseUri);

        /// <summary>
        /// Private variable holding the image URL
        /// </summary>
        private string imageUrl;
        
        /// <summary>
        /// Gets or sets the ImageUrl
        /// </summary>
        [DataMember(
          Name = "ImageUrl",
          Order = 1,
          IsRequired = true)]
        public string ImageUrl
        {
            get
            {
                return this.imageUrl;
            }

            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    this.imageUrl = value;
                }
                else
                {                 
                    string[] pieces = value.Split('~');
                    this.imageUrl = pieces.Length > 1 ?
                        string.Format(
                        imageUrlTemplate,
                        pieces[1]) : (!value.Contains(imageBase) ? imageBase + value :
                        value);
                }
            }
        }
    }
}
