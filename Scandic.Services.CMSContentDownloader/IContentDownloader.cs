﻿// <copyright file="IContentDownloader.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.ContentDownloader
{
    #region references

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Scandic.Services.Framework;
    using Scandic.Services.BusinessServices;

    #endregion

    /// <summary>
    /// Enumeration for frequency for content download.
    /// </summary>
    public enum DownloadFrequency
    {
        /// <summary>
        /// Content to be dowloaded frequently i.e. more than once a day.
        /// </summary>
        FREQUENT,

        /// <summary>
        /// Content to be downloaded once a day.
        /// </summary>
        DAILY
    }

    /// <summary>
    /// Interface to be implemented by classes that download content from CMS
    /// </summary>
    public interface IContentDownloader
    {
        /// <summary>
        /// Method to be invoked to download content.
        /// </summary>
        void DowloadContent();

        /// <summary>
        /// Frequency for downloading content.
        /// </summary>
        DownloadFrequency ContentDownloadFrequency { get; }
    }

    /// <summary>
    /// Class for downloading network content.
    /// </summary>
    public class HotelNetworkContentDownloader : IContentDownloader
    {
        /// <summary>
        /// Donwnload frequency for network content.
        /// </summary>
        public DownloadFrequency ContentDownloadFrequency { get; private set; }

        public HotelNetworkContentDownloader() 
        {
            this.ContentDownloadFrequency = DownloadFrequency.DAILY;
        }

        /// <summary>
        /// Download network content
        /// </summary>
        public void DowloadContent()
        {
            string[] languages = ConfigHelper.GetValue(ConfigKeys.ValidLanguages).Split(',');
            string provider = ConfigHelper.GetValue(ConfigKeys.Provider);

            foreach (string language in languages)
            {
                try
                {
                    LogHelper.LogInfo(
                    string.Format("Downloading Content: Network | {0} | {1}", provider, language),
                    LogCategory.ContentDownloader);
                    new HotelNetworkManager(language, provider).DownloadHotelNetwork();
                }
                catch (Exception ex)
                {
                    LogHelper.LogException(ex, LogCategory.ContentDownloader);
                    throw ex;
                }
            }
        }        
    }

    /// <summary>
    /// Class for downloading hotel details.
    /// </summary>
    public class HotelDetailsContentDownloader : IContentDownloader
    {
        /// <summary>
        /// Donwnload frequency for network content.
        /// </summary>
        public DownloadFrequency ContentDownloadFrequency { get; private set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public HotelDetailsContentDownloader() 
        {
            this.ContentDownloadFrequency = DownloadFrequency.DAILY;
        }

        /// <summary>
        /// Downloads hotel details
        /// </summary>
        public void DowloadContent()
        {
            string[] languages = ConfigHelper.GetValue(ConfigKeys.ValidLanguages).Split(',');
            string provider = ConfigHelper.GetValue(ConfigKeys.Provider);

            foreach (string language in languages)
            {
                try
                {
                    LogHelper.LogInfo(
                    string.Format("Downloading Content: Hotel Details | {0} | {1}", provider, language),
                    LogCategory.ContentDownloader);
                    new HotelDetailsManager(language, provider).DownloadHotelPages();
                }
                catch (Exception ex)
                {
                    LogHelper.LogException(ex, LogCategory.ContentDownloader);
                    throw ex;
                }
            }

        }        
    }

    /// <summary>
    /// Class for downloading rate types.
    /// </summary>
    public class RateTypeContentDownloader : IContentDownloader
    {
        /// <summary>
        /// Donwnload frequency for network content.
        /// </summary>
        public DownloadFrequency ContentDownloadFrequency { get; private set; }

        /// <summary>
        /// Constructor.
        /// </summary>
        public RateTypeContentDownloader()
        {
            this.ContentDownloadFrequency = DownloadFrequency.FREQUENT;
        }

        /// <summary>
        /// Downloads rate types.
        /// </summary>
        public void DowloadContent()
        {
            string[] languages = ConfigHelper.GetValue(ConfigKeys.ValidLanguages).Split(',');
           
            foreach (string language in languages)
            {
                try
                {
                    LogHelper.LogInfo(
                    string.Format("Downloading Content: Rate Types | {0} ", language),
                    LogCategory.ContentDownloader);
                    new RateTypeManager(language).DownloadRateTypeDetails();
                }
                catch (Exception ex)
                {
                    LogHelper.LogException(ex, LogCategory.ContentDownloader);
                    throw ex;
                }
            }

        }
    }

    /// <summary>
    /// Class for downloading room types.
    /// </summary>
    public class RoomTypeContentDownloader : IContentDownloader
    {
        /// <summary>
        /// Donwnload frequency for network content.
        /// </summary>
        public DownloadFrequency ContentDownloadFrequency { get; private set; }

        /// <summary>
        /// Constructor.
        /// </summary>
        public RoomTypeContentDownloader()
        {
            this.ContentDownloadFrequency = DownloadFrequency.DAILY;
        }

        /// <summary>
        /// Downloads room types.
        /// </summary>
        public void DowloadContent()
        {
            string[] languages = ConfigHelper.GetValue(ConfigKeys.ValidLanguages).Split(',');

            foreach (string language in languages)
            {
                try
                {
                    LogHelper.LogInfo(
                    string.Format("Downloading Content: Rate Types | {0} ", language),
                    LogCategory.ContentDownloader);
                    new RoomTypeManager(language).DownloadRoomTypeDetails();
                }
                catch (Exception ex)
                {
                    LogHelper.LogException(ex, LogCategory.ContentDownloader);
                    throw ex;
                }
            }

        }
    }

    /// <summary>
    /// Class for downloading partner details.
    /// </summary>
    public class PartnerContentDownloader : IContentDownloader
    {
        /// <summary>
        /// Donwnload frequency for network content.
        /// </summary>
        public DownloadFrequency ContentDownloadFrequency { get; private set; }

        /// <summary>
        /// Constructor.
        /// </summary>
        public PartnerContentDownloader()
        {
            this.ContentDownloadFrequency = DownloadFrequency.FREQUENT;
        }

        /// <summary>
        /// Downloads room types.
        /// </summary>
        public void DowloadContent()
        {
            try
            {
               LogHelper.LogInfo(
               string.Format("Downloading Content: Partners"),
               LogCategory.ContentDownloader);
               new PartnerManager().DownloadPartners();
            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex, LogCategory.ContentDownloader);
                throw ex;
            }          

        }
    }
}
