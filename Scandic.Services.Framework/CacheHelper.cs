﻿// <copyright file="CacheHelper.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.Framework
{
    using System;
    using Microsoft.Practices.EnterpriseLibrary.Caching;
    using Microsoft.Practices.EnterpriseLibrary.Caching.Expirations;

    #region Enum
    /// <summary>
    /// Friendly form of durations
    /// </summary>
    public enum CacheDuration
    {
        /// <summary>
        /// Cache duration of one hour
        /// </summary>
        Hour,

        /// <summary>
        /// Cache duration of half a day
        /// </summary>
        HalfDay,

        /// <summary>
        /// Cache duration of one day
        /// </summary>
        Day,

        /// <summary>
        /// Cache duration of one week
        /// </summary>
        Week
    }
    #endregion

    /// <summary>
    /// Helper class to provide easy to use
    /// static methods to store to and retrieve from cache.
    /// Only Put, Get and availability check are implemented
    /// for keeping the helper clean. Flush/Remove need to
    /// be accessed directly on the Enterprise Library.
    /// </summary>
    public static class CacheHelper
    {
        public static int CacheCount()
        {
            return CacheFactory.GetCacheManager().Count;
        }

        #region Removal / Clearing
        /// <summary>
        /// Removes an item from the Cache
        /// </summary>
        /// <param name="key">Key of the item to be removed</param>
        public static void Clear(string key)
        {
            CacheFactory.GetCacheManager()
                .Remove(key);
            LogHelper.LogInfo("Remove Cache: " + key, LogCategory.Service);
        }

        /// <summary>
        /// Flushes the Cache
        /// i.e. removes all items from the cache
        /// </summary>
        /// <returns> Returns the number of items currently in the cache </returns>
        public static int Flush()
        {
            int count = CacheFactory.GetCacheManager().Count;
            CacheFactory.GetCacheManager()
                .Flush();
            LogHelper.LogInfo("Flush Cache", LogCategory.Service);
            return count;
        }
        #endregion

        #region Interval Expiry
        #region Default Cache
        /// <summary>
        /// Puts an object to cache, with
        /// specified key and for specified duration
        /// </summary>
        /// <param name="key">Key to identify the object in cache</param>
        /// <param name="value">The object to be stored in cache</param>
        /// <param name="minutes">The duration in minutes for which object is to be cached</param>
        public static void Put(string key, object value, long minutes)
        {
            LogHelper.LogInfo("To Cache: " + key, LogCategory.Service);
            CacheFactory.GetCacheManager()
                .Add(key, value, CacheItemPriority.Normal, null, new SlidingTime(TimeSpan.FromMinutes(minutes)));
        }

        /// <summary>
        /// Puts an object to cache, with file dependency
         /// </summary>
        /// <param name="key">Key to identify the object in cache</param>
        /// <param name="value">The object to be stored in cache</param>
        /// <param name="timestampfile">The name of the timestamp file</param>
        public static void Put(string key, object value, string timestampfile)
        {
            LogHelper.LogInfo("To Cache: " + key, LogCategory.Service);
            CacheFactory.GetCacheManager()
                .Add(key, value, CacheItemPriority.Normal, null, new FileDependency(timestampfile));
        }

        /// <summary>
        /// Puts an object to cache, with
        /// specified key and for specified duration
        /// </summary>
        /// <param name="key">Key to identify the object in cache</param>
        /// <param name="value">The object to be stored in cache</param>
        /// <param name="duration">The duration for which the object is to be cached</param>
        public static void Put(string key, object value, CacheDuration duration)
        {
            switch (duration)
            {
                case CacheDuration.Hour:
                    Put(key, value, 60);
                    break;
                case CacheDuration.HalfDay:
                    Put(key, value, 720);
                    break;
                case CacheDuration.Day:
                    Put(key, value, 1440);
                    break;
                case CacheDuration.Week:
                    Put(key, value, 10080);
                    break;
            }
        }

        /// <summary>
        /// Retrieve an object from cache
        /// </summary>
        /// <param name="key">Key of the object to be retrieved</param>
        /// <returns>The requested object, from cache</returns>
        public static object Get(string key)
        {
            LogHelper.LogInfo("From Cache: " + key, LogCategory.Service);
            return CacheFactory.GetCacheManager().GetData(key);
        }

        /// <summary>
        /// Checks if an object corresponding to key
        /// is available in cache
        /// </summary>
        /// <param name="key">The key for which check is to be carried out</param>
        /// <returns>True for object for key being available, and vice-versa</returns>
        public static bool IsAvailable(string key)
        {
            return CacheFactory.GetCacheManager().Contains(key);
        }
        #endregion

        #region Named Cache
        /// <summary>
        /// Puts an object to cache, with
        /// specified key and for specified duration
        /// </summary>
        /// <param name="key">Key to identify the object in cache</param>
        /// <param name="value">The object to be stored in cache</param>
        /// <param name="minutes">The duration in minutes for which object is to be cached</param>
        /// <param name="cacheManager">Cache Manager instance name</param>
        public static void Put(string key, object value, long minutes, string cacheManager)
        {
            LogHelper.LogInfo("To Cache: " + key, LogCategory.Service);
            CacheFactory.GetCacheManager(cacheManager)
                .Add(key, value, CacheItemPriority.Normal, null, new SlidingTime(TimeSpan.FromMinutes(minutes)));
        }

        /// <summary>
        /// Puts an object to cache, with
        /// specified key and for specified duration
        /// </summary>
        /// <param name="key">Key to identify the object in cache</param>
        /// <param name="value">The object to be stored in cache</param>
        /// <param name="duration">The duration for which the object is to be cached</param>
        /// <param name="cacheManager">Cache Manager instance name</param>
        public static void Put(string key, object value, CacheDuration duration, string cacheManager)
        {
            switch (duration)
            {
                case CacheDuration.Hour:
                    Put(key, value, 60, cacheManager);
                    break;
                case CacheDuration.HalfDay:
                    Put(key, value, 720, cacheManager);
                    break;
                case CacheDuration.Day:
                    Put(key, value, 1440, cacheManager);
                    break;
                case CacheDuration.Week:
                    Put(key, value, 10080, cacheManager);
                    break;
            }
        }

        /// <summary>
        /// Retrieve an object from cache
        /// </summary>
        /// <param name="key">Key of the object to be retrieved</param>
        /// <param name="cacheManager">Cache Manager instance name</param>
        /// <returns>The requested object, from cache</returns>
        public static object Get(string key, string cacheManager)
        {
            LogHelper.LogInfo("From Cache: " + key, LogCategory.Service);
            return CacheFactory.GetCacheManager(cacheManager).GetData(key);
        }

        /// <summary>
        /// Checks if an object corresponding to key
        /// is available in cache
        /// </summary>
        /// <param name="key">The key for which check is to be carried out</param>
        /// <param name="cacheManager">Cache Manager instance name</param>
        /// <returns>True for object for key being available, and vice-versa</returns>
        public static bool IsAvailable(string key, string cacheManager)
        {
            return CacheFactory.GetCacheManager(cacheManager).Contains(key);
        }
        #endregion
        #endregion

        #region Scheduled Expiry
        #region Default Cache
        /// <summary>
        /// The cached item expires after the
        /// configured daily job end time
        /// </summary>
        /// <param name="key">Key to identify the object in cache</param>
        /// <param name="value">The object to be stored in cache</param>
        public static void PutTillDailyRefresh(string key, object value)
        {
            /*
             * "* * * * *"      every minute 
             * "5 * * * *"      5th minute of every hour 
             * "* 21 * * *"     every minute of the 21st hour of every day 
             * "31 15 * * *"    3:31 PM every day 
             * "7 4 * * 6"      Saturday 4:07 AM 
             * "15 21 4 7 *"    9:15 PM on 4 July
             */
            LogHelper.LogInfo("To Cache: " + key, LogCategory.Service);
            Put(key, value, CacheDuration.Day);
            /*
            CacheFactory.GetCacheManager().Add(
                key,
                value,
                CacheItemPriority.Normal,
                null,
                new ExtendedFormatTime(ConfigHelper.GetValue(ConfigKeys.DailyExpiry)));
             */
        }

        /// <summary>
        /// The cached item expires after the
        /// configured weekly job end time
        /// </summary>
        /// <param name="key">Key to identify the object in cache</param>
        /// <param name="value">The object to be stored in cache</param>
        public static void PutTillWeeklyRefresh(string key, object value)
        {
            LogHelper.LogInfo("To Cache: " + key, LogCategory.Service);
            CacheFactory.GetCacheManager().Add(
                key,
                value,
                CacheItemPriority.Normal,
                null,
                new ExtendedFormatTime(ConfigHelper.GetValue(ConfigKeys.WeeklyExpiry)));
        }
   
        #endregion

        #region Named Cache
        /// <summary>
        /// The cached item expires after the
        /// configured daily job end time
        /// </summary>
        /// <param name="key">Key to identify the object in cache</param>
        /// <param name="value">The object to be stored in cache</param>
        /// <param name="cacheManager">Cache Manager instance name</param>
        public static void PutTillDailyRefresh(string key, object value, string cacheManager)
        {
            CacheFactory.GetCacheManager(cacheManager).Add(
                key,
                value,
                CacheItemPriority.Normal,
                null,
                new ExtendedFormatTime(ConfigHelper.GetValue(ConfigKeys.DailyExpiry)));
        }

        /// <summary>
        /// The cached item expires after the
        /// configured weekly job end time
        /// </summary>
        /// <param name="key">Key to identify the object in cache</param>
        /// <param name="value">The object to be stored in cache</param>
        /// <param name="cacheManager">Cache Manager instance name</param>
        public static void PutTillWeeklyRefresh(string key, object value, string cacheManager)
        {
            CacheFactory.GetCacheManager(cacheManager).Add(
                key,
                value,
                CacheItemPriority.Normal,
                null,
                new ExtendedFormatTime(ConfigHelper.GetValue(ConfigKeys.WeeklyExpiry)));
        }
        #endregion
        #endregion
    }
}