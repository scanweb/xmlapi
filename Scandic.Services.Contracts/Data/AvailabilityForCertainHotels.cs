﻿// <copyright file="AvailabilityForCertainHotels.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.Contracts.Data
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using Scandic.Services.Contracts.Data;
    using Scandic.Services.Contracts.Operation;
    using Biz = Scandic.Services.BusinessEntity;

    /// <summary>
    /// Reply Object for AvailabilityForCertainHotels
    /// </summary>    
    [DataContract(Name = "HotelsAvailability", Namespace = @"http://api.scandichotels.com/schemas")]
    public class AvailabilityForCertainHotels
    {
        /// <summary>
        /// Gets or sets the Name of the Operator (i.e. Scandic)
        /// </summary>
        [DataMember(
            Name = "Language",
            Order = 0,
            IsRequired = true)]
        public string Language { get; set; }

        /// <summary>
        /// Gets or sets the Name of the Operator (i.e. Scandic)
        /// </summary>
        [DataMember(
            Name = "Operator",
            Order = 1,
            IsRequired = true)]
        public string Operator { get; set; }

        /// <summary>
        /// Gets or sets the name of the MessageOnSingleHotelSearchNotAvailable
        /// </summary>
        [DataMember(Name = "MessageonOnSingleHotelSearchNotAvailable", Order = 3, IsRequired = true)]
        public string MessageonOnSingleHotelSearchNotAvailable { get; set; }
 
        /// <summary>
        /// Gets or sets the list of cities matching the query
        /// </summary>
        [DataMember(
            Name = "Hotels",
            EmitDefaultValue = false,
            Order = 2,
            IsRequired = true)]
        public IList<AvailableHotelDetails> Hotels { get; set; }

        /// <summary>
        /// Implicit casting conversion operator
        /// </summary>
        /// <param name="hotelList">HotelDetails business entity</param>
        /// <returns>Converted HotelDetails data contract</returns>
        public static implicit operator AvailabilityForCertainHotels(Biz.AvailableHotelDetailsList hotelList)
        {
            if (hotelList == null)
            {
                return null;
            }

            AvailabilityForCertainHotels thisHotel = new AvailabilityForCertainHotels
            {
              Operator = hotelList.Provider,
              Hotels = new List<AvailableHotelDetails>(),
              Language = hotelList.Language,
              MessageonOnSingleHotelSearchNotAvailable = hotelList.MessageOnSingleHotelSearchNotAvailable
             };

            foreach (AvailableHotelDetails h in hotelList.AvailableHotels)
            {
                thisHotel.Hotels.Add(new AvailableHotelDetails
                {
                    Continent = h.Continent,
                    HotelId = h.HotelId,
                    HotelName = h.HotelName,
                    HotelDescription = h.HotelDescription,
                    HotelAddress = h.HotelAddress,
                    FromRate = h.FromRate,
                    Image = h.Image,
                    DeeplinkURLHotelPage = h.DeeplinkURLHotelPage
                });
            }

            return thisHotel;
        }
    }
}
