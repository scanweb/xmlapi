﻿// <copyright file="RoomAndRate.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.Contracts.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    using Biz = Scandic.Services.BusinessEntity;

    [DataContract(
    Name = "RoomAndRate",
    Namespace = @"http://api.scandichotels.com/schemas")]

    /// <summary>
    /// Class to hold Room and Rate information
    /// </summary>
    public class RoomAndRateWithTaxes : RoomAndRate
    {
        
        /// <summary>
        /// Gets or sets TotalPricePerStay
        /// </summary>          
        [DataMember(
       Name = "TotalBaseRatePerStay",
       Order = 3,
       IsRequired = true)]
        public double TotalBaseRatePerStay { get; set; }

        /// <summary>
        /// Gets or sets TotalPricePerNight
        /// </summary>        
        [DataMember(
       Name = "TotalBaseRatePerNight",
       Order = 4,
       IsRequired = true)]
        public double TotalBaseRatePerNight { get; set; }

        /// <summary>
        /// Gets or Sets Taxes 
        /// </summary>
        [DataMember(
       Name = "Taxes",
       Order = 5,
       IsRequired = true)]
        public List<Tax> Taxes { get; set; }
               
    }
}
