﻿// <copyright file="RoomFacilities.cs" company="Scandic Hotels">
// Copyright Scandic. All rights reserved.
// </copyright>

namespace Scandic.Services.Contracts.Data
{
    using System.Runtime.Serialization;

    /// <summary>
    /// This class has fields to hold the Facilities available in the hotelroom
    /// </summary>   
      [DataContract(
        Name = "RoomFacility",
        Namespace = @"http://api.scandichotels.com/schemas")]
    public class RoomFacilities
    {
        /// <summary>           
        /// Gets or sets the Latitude 
        /// </summary> 
        [DataMember(
            Name = "Name",
            Order = 0,
            IsRequired = true)]
        public string Name { get; set; }

        /// <summary>           
        /// Gets or sets the Latitude 
        /// </summary>
        [DataMember(
            Name = "Description",
            Order = 1,
            IsRequired = true)] 
        public string Description { get; set; }

        /// <summary>           
        /// Gets or sets the Image details 
        /// </summary> 
         [DataMember(
            Name = "Image",
            Order = 2,
            IsRequired = true)] 
        public RoomImage Image { get; set; }
    }
}
