﻿
// <copyright file="APIKeyInspector.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.Operations.Extensions
{
    using System.Net;
    using System.ServiceModel;
    using System.ServiceModel.Dispatcher;
 
    /// <summary>
    /// class to inspect request header.
    /// </summary>
    /// <remarks></remarks>
    public class APIKeyInspector : IDispatchMessageInspector
    {
        /// <summary>
        /// Called after an inbound message has been received but before the message is dispatched to the intended operation.
        /// </summary>
        /// <param name="request">The request message.</param>
        /// <param name="channel">The incoming channel.</param>
        /// <param name="instanceContext">The current service instance.</param>
        /// <returns>The object used to correlate state. This object is passed back in the <see cref="M:System.ServiceModel.Dispatcher.IDispatchMessageInspector.BeforeSendReply(System.ServiceModel.Channels.Message@,System.Object)"/> method.</returns>
        /// <remarks></remarks>
        public object AfterReceiveRequest(ref System.ServiceModel.Channels.Message request, System.ServiceModel.IClientChannel channel, System.ServiceModel.InstanceContext instanceContext)
        {
            this.SendRequestToSiteCatalyst();
            /*
      var context = OperationContext.Current;
      string apiname = context.IncomingMessageHeaders.To.PathAndQuery.Remove(0, 1).Split('?')[0];
      string rightpart = string.Format("b/ss/schabxmlapidev/0?gn={0}|{1}&vid={0}&ns=Scandichotels&c24={0}&v49={0}&c25={1}&v50={1}&c3=Weekend&v3=Weekend&c5=Sunday&v22=Sunday&c6=9%3A30AM&v23=9%3A30AM&ev=event50%2Cevent1", "100", apiname);
            
      string urltositeCatelyst = string.Format("{0}/{1}", leftpart, rightpart);
      
       int index = OperationContext.Current.IncomingMessageHeaders.FindHeader("Date", "ns");
       if (index >= 0)
       {
           string value = OperationContext.Current.IncomingMessageHeaders.GetHeader<string>(index);
       }

       object prop;
       string myHeader;

       if (!OperationContext.Current.IncomingMessageProperties.TryGetValue(HttpRequestMessageProperty.Name, out prop))
       {
           myHeader = "Cannot read header from request";
       }
       else
       {
           HttpRequestMessageProperty reqProp = (HttpRequestMessageProperty)prop;
           myHeader = reqProp.Headers["X-MyHeader"];
           Console.WriteLine("X-MyHeader: {0}", myHeader);
       }
       return null;

       object prop;
       string result = string.Empty;
       if (OperationContext.Current.IncomingMessageProperties.TryGetValue(HttpRequestMessageProperty.Name, out prop))
       {
           HttpRequestMessageProperty httpProp = (HttpRequestMessageProperty)prop;
           string headerValue = httpProp.Headers["H1"];
           if (String.IsNullOrEmpty(headerValue))
           {
               throw new FaultException<ServiceFault>(
              new ServiceFault { ErrorCode = "1004", HttpErrorCode = "500", ErrorMessage = "No Header Specified" },
              new FaultReason("No Header Specified"));

           }
           else
           {
               Console.WriteLine("\n{0}\n", headerValue);
           }


       }*/

            /*
            MessageBuffer buffer = request.CreateBufferedCopy(Int32.MaxValue);
            request = buffer.CreateMessage();
            Message originalMessage = buffer.CreateMessage();
            MessageHeader GUID = null;
            foreach (MessageHeader h in originalMessage.Headers)
            {
                GUID = h;
                Console.WriteLine("\n{0}\n", h);
            }

            return null;
            */

            return null;
        }

        /// <summary>
        /// Called after the operation has returned but before the reply message is sent.
        /// </summary>
        /// <param name="reply">The reply message. This value is null if the operation is one way.</param>
        /// <param name="correlationState">The correlation object returned from the <see cref="M:System.ServiceModel.Dispatcher.IDispatchMessageInspector.AfterReceiveRequest(System.ServiceModel.Channels.Message@,System.ServiceModel.IClientChannel,System.ServiceModel.InstanceContext)"/> method.</param>
        /// <remarks></remarks>
        public void BeforeSendReply(ref System.ServiceModel.Channels.Message reply, object correlationState)
        {
        }

        /// <summary>
        /// Sends the request to site catalyst.
        /// </summary>
        /// <remarks></remarks>
        private void SendRequestToSiteCatalyst()
        {
            OperationContext context = OperationContext.Current;
            string apiname = context.IncomingMessageHeaders.To.PathAndQuery.Remove(0, 1).Split('?')[0];
            string leftpart = "http://localhost/website1";
            string rightPart = this.SetVariablesForSiteCatalystForContentService(apiname);
            string urltositeCatelyst = string.Format("{0}/{1}", leftpart, rightPart);
            WebRequest requestToSitecatalyst = WebRequest.Create(urltositeCatelyst);

            // execute the request
            requestToSitecatalyst.Credentials = CredentialCache.DefaultCredentials;
            //requestToSitecatalyst.GetResponse();
        }

        /// <summary>
        /// Gets the name of the site catalyst event.
        /// </summary>
        /// <param name="apiname">The apiname.</param>
        /// <returns> returns site catalyst event name </returns>
        /// <remarks></remarks>
        private string GetSiteCatalystEventName(string apiname)
        {
            string eventID = string.Empty;
            switch (apiname)
            {
                case "countries":
                    {
                        eventID = "event1";
                        break;
                    }

                case "cities":
                    {
                        eventID = "event2";
                        break;
                    }

                case "hotels":
                    {   
                        eventID = "event3";
                        break;
                    }

                case "hotel":
                    {
                        eventID = "event4";
                        break;
                    }
            }

            return eventID;
        }

        /// <summary>
        /// Sets the variables for site catalyst for content service.
        /// </summary>
        /// <param name="apiname">The apiname.</param>
        /// <returns> returns formatted string for the right part of the sitecatalyst Url.</returns>
        /// <remarks></remarks>
        private string SetVariablesForSiteCatalystForContentService(string apiname)
        {
            string siteCatalystEventId = this.GetSiteCatalystEventName(apiname);
            string rightpart = string.Format("?gn={0}|{1}&vid={0}&ns=scandichotelsab&c24={0}&v49={0}&c25={1}&v50={1}&c3=Weekend&v3=Weekend&c5=Sunday&v22=Sunday&c6=9%3A30AM&v23=9%3A30AM&ev=event50%2C{2}", "Destinationse", apiname, siteCatalystEventId);
            return rightpart;
        }

        /// <summary>
        /// Sets the variables for site catalyst for availability service.
        /// </summary>
        /// <param name="apiname">The apiname.</param>
        /// <returns> returns formatted string for the right part of the sitecatalyst Url. </returns>
        /// <remarks></remarks>
        private string SetVariablesForSiteCatalystForAvailabilityService(string apiname)
        {
            string siteCatalystEventId = this.GetSiteCatalystEventName(apiname);
            string rightpart = string.Format("?gn={0}|{1}&vid={0}&ns=Scandichotels&c24={0}&v49={0}&c25={1}&v50={1}&c3=Weekend&v3=Weekend&c5=Sunday&v22=Sunday&c6=05%3A00PM&v23=05%3A00PM&ev=event50%2Cevent6&c27=no&v47=no&pl=%3B830%2C%3B827%2C%3B822&c14=2011-07-21&v14=2011-07-21&c17=2011-07-21-2011-07-25&v17=2011-07-21-2011-07-25&c13=3&v13=3&c15=7&v15=7&c16=2&v16=2&c12=4&v12=4&c51=3&v51=3", "Destinationse", apiname, siteCatalystEventId);
            return rightpart;
        }
    }
}
