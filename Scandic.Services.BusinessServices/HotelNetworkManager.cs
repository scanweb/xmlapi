﻿// <copyright file="HotelNetworkManager.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.BusinessServices
{
    #region References

    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Threading.Tasks;
    using System.Xml;
    using Scandic.Services.BusinessEntity;
    using Scandic.Services.Framework;
    using Scandic.Services.ServiceAgents;
    using Scandic.Services.ServiceAgents.Cms.CmsProxy;

    #endregion

    /// <summary>
    /// Manages the HotelNetwork data
    /// </summary>
    public class HotelNetworkManager : EntityManagerBase
    {
        /// <summary>
        /// Lock object
        /// </summary>
        private static Object lockObject = new Object();

        /// <summary>
        /// Initializes a new instance of the HotelNetworkManager class
        /// </summary>
        /// <param name="language">Language for this operation</param>
        /// <param name="provider">Operator for this operation</param>
        public HotelNetworkManager(string language, string provider)
        {
            this.Network = new HotelNetwork { Language = language, Provider = provider };
            this.Language = language;
            this.CacheKey = string.Format(
                CultureInfo.InvariantCulture,
                "{0}-{1}-{2}",
                "HotelNetworkManager",
                this.Network.Provider,
                this.Network.Language);
            this.Initialize();
        }

        /// <summary>
        /// Gets the hotel network to be populated and managed
        /// </summary>
        public HotelNetwork Network { get; private set; }

        /// <summary>
        /// Helper method to fill the hotel network tree
        /// </summary>
        public override void SetManagedEntity()
        {
            bool refreshEntity = !(this.IsCacheValid);

            if (!refreshEntity)
            {
                this.Network = this.CachedValue as HotelNetwork;
            }

            refreshEntity = refreshEntity || this.Network == null;

            // If an entity needs to be refreshed i.e.
            // it is not in cache but needs to be picked
            // from the offline file.
            if (refreshEntity)
            {
                // There can be more than one thread trying to refresh
                // So, allow only one thread to pick data from file.
                lock (lockObject)
                {
                    if (this.CachedValue == null)
                    {
                        var entity = this.RefreshEntity<HotelNetwork>();
                        if (!(entity == default(HotelNetwork)))
                        {
                            this.Network = entity;
                        }
                        if (this.IsCached)
                        {
                            this.CachedValue = this.Network;
                        }
                    }
                    else
                    {
                        this.Network = this.CachedValue as HotelNetwork;
                        LogHelper.LogInfo("Network already loaded by another thread", LogCategory.Content);
                    }
                }
            }
        }

        /// <summary>
        /// Gets the value of a node
        /// </summary>
        /// <param name="parent">parent node in which to search</param>
        /// <param name="node">node name to search</param>
        /// <returns>cleaned up value of the node</returns>
        private static string GetNodeValue(XmlNode parent, string node)
        {
            if (parent.SelectSingleNode(node) != null)
            {
                return parent.SelectSingleNode(node).InnerText;
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the continent list.
        /// </summary>
        /// <param name="countryPages">The country pages.</param>
        /// <returns>List of continenets configured in CMS</returns>
        private static List<Continent> GetContinentList(RawPage[] countryPages)
        {
            List<Continent> continentList = new List<Continent>();
            foreach (RawPage countryPage in countryPages)
            {
                if (ContentHelper.IsPagePublished(countryPage))
                {
                    bool isNewContinent = true;
                    // Fix for the defect : artf1231721 
                    string continentName = string.IsNullOrEmpty(ContentHelper.GetPropertyValue(countryPage, "Continent")) ? "Europe" : ContentHelper.GetPropertyValue(countryPage, "Continent");
                    foreach (Continent continent in continentList)
                    {
                        if (continent.Name.Equals(continentName, StringComparison.InvariantCultureIgnoreCase))
                        {
                            isNewContinent = false;
                            break;
                        }
                    }

                    if (isNewContinent)
                    {
                        Continent continent = new Continent();
                        continent.Id = continentName;
                        continent.Name = continentName;
                        continentList.Add(continent);
                    }
                }
            }

            return continentList;
        }

        /// <summary>
        /// Download the hotel network.
        /// </summary>
        public void DownloadHotelNetwork()
        {
            Stopwatch watch = new Stopwatch();
            int rootPageReferenceID = Convert.ToInt32(ConfigHelper.GetValue(ConfigKeys.RootPageReferenceID));
            string countryPageTypeID = ConfigHelper.GetValue(ConfigKeys.CountryPageTypeID);
            string cityPageTypeID = ConfigHelper.GetValue(ConfigKeys.CityPageTypeID);
            string hotelPageTypeID = ConfigHelper.GetValue(ConfigKeys.HotelPageTypeID);
            string hotelsToExclude = !string.IsNullOrEmpty(ConfigHelper.GetValue(ConfigKeys.HotelsToExclude)) ? ConfigHelper.GetValue(ConfigKeys.HotelsToExclude) : string.Empty;
            this.Network = new HotelNetwork { Language = this.Language, Provider = this.Network.Provider };
            if (PerformanceHelper.Enabled)
            {
                watch.Start();
            }

            RawPage[] countryPages = ContentAccess.GetPagesWithPageTypeID(countryPageTypeID, rootPageReferenceID, this.Network.Language);
            if (ContentHelper.IsPagePublished(countryPages[0]))
            {
                this.Network.Continents.Add(
                    new Continent
                    {
                        Id = ContentHelper.GetPropertyValue(countryPages[0], "Continent"),
                        Name = ContentHelper.GetPropertyValue(countryPages[0], "Continent")
                    });
            }

            if (bool.Parse(ConfigHelper.GetValue(ConfigKeys.EnableContentThreads)))
            {
                List<Exception> exceptions = null;
                Parallel.ForEach(
                    countryPages,
                    countryPage =>
                    {
                        string language = this.Network.Language;
                        try
                        {
                            if (ContentHelper.IsPagePublished(countryPage))
                            {
                                string currentContinentName = ContentHelper.GetPropertyValue(countryPage, "Continent");
                                int countryPageReferenceID = Convert.ToInt32(ContentHelper.GetPropertyValue(countryPage, "PageLink"));
                                string strAPITaxRates = ContentHelper.GetPropertyValue(countryPage, "APITaxRates");
                                double apitaxRates;
                                Double.TryParse(strAPITaxRates, out apitaxRates);
                                Country country = new Country
                                {
                                    Id = ContentHelper.GetPropertyValue(countryPage, "CountryCode"),
                                    Name = ContentHelper.GetPropertyValue(countryPage, "PageName"),
                                    Continent = currentContinentName,
                                    APITaxRates = apitaxRates
                                };
                                LogHelper.LogInfo(string.Format("Country{0,15}", country.Name), LogCategory.Service);
                                RawPage[] cityPages = ContentAccess.GetPagesWithPageTypeID(cityPageTypeID, countryPageReferenceID, language);
                                Parallel.ForEach(
                                    cityPages,
                                    cityPage =>
                                    {
                                        try
                                        {
                                            if (ContentHelper.IsPagePublished(cityPage))
                                            {
                                                int cityPageReferenceID = Convert.ToInt32(ContentHelper.GetPropertyValue(cityPage, "PageLink"));
                                                City city = new City()
                                                {
                                                    Id = ContentHelper.GetPropertyValue(cityPage, "OperaID"),
                                                    Name = ContentHelper.GetPropertyValue(cityPage, "PageName")
                                                };
                                                LogHelper.LogInfo(string.Format("City{0,18}{1,15}", country.Name, city.Name), LogCategory.Service);
                                                RawPage[] hotelPages = ContentAccess.GetPagesWithPageTypeID(hotelPageTypeID, cityPageReferenceID, language);
                                                Parallel.ForEach(
                                                    hotelPages,
                                                    hotelPage =>
                                                    {
                                                        try
                                                        {
                                                            if (ContentHelper.IsPagePublished(hotelPage))
                                                            {
                                                                if (!hotelsToExclude.Contains(";" + ContentHelper.GetPropertyValue(hotelPage, "OperaID") + ";"))
                                                                {
                                                                    Hotel hotel = new Hotel
                                                                    {
                                                                        Id = ContentHelper.GetPropertyValue(hotelPage, "OperaID"),
                                                                        Name = ContentHelper.GetPropertyValue(hotelPage, "Heading"),
                                                                        PropertyType = ContentHelper.GetPropertyValue(hotelPage, "HotelCategory"),
                                                                        PostalCity = ContentHelper.GetPropertyValue(hotelPage, "PostalCity"),
                                                                        IsBookable = (!string.IsNullOrEmpty(ContentHelper.GetPropertyValue(hotelPage, "AvailableInBooking")))
                                                                        && Convert.ToBoolean(ContentHelper.GetPropertyValue(hotelPage, "AvailableInBooking")),
                                                                        APITaxRates = country.APITaxRates
                                                                    };
                                                                    LogHelper.LogInfo(string.Format("Hotel{0,17}{1,15}{2,30}", country.Name, city.Name, hotel.Name), LogCategory.Service);
                                                                    city.Hotels.Add(hotel);
                                                                }
                                                            }
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            if (exceptions == null)
                                                            {
                                                                exceptions = new List<Exception>();
                                                            }
                                                            exceptions.Add(ex);
                                                        }
                                                        finally
                                                        {
                                                            if (exceptions != null) throw new AggregateException(exceptions);
                                                        }
                                                    });
                                                country.Cities.Add(city);
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            if (exceptions == null)
                                            {
                                                exceptions = new List<Exception>();
                                            }
                                            exceptions.Add(ex);
                                        }
                                        finally
                                        {
                                            if (exceptions != null) throw new AggregateException(exceptions);
                                        }
                                    });

                                Continent currentContinent = new Continent
                                    {
                                        Id = currentContinentName,
                                        Name = currentContinentName,
                                    };

                                int index = this.Network.Continents.IndexOf(currentContinent);
                                if (index >= 0)
                                {
                                    this.Network.Continents[index].Countries.Add(country);
                                }
                                else
                                {
                                    currentContinent.Countries.Add(country);
                                    this.Network.Continents.Add(currentContinent);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            if (exceptions == null)
                            {
                                exceptions = new List<Exception>();
                            }
                            exceptions.Add(ex);
                        }
                        finally
                        {
                            if (exceptions != null) throw new AggregateException(exceptions);
                        }
                    });

                Serializer.ObjectToFile(this.OfflineFile, this.Network);
            }
            else
            {
                this.FillHotelNetwork(this.Network.Language, this.Network.Provider);
            }

            if (PerformanceHelper.Enabled)
            {
                watch.Stop();
                PerformanceHelper.RegisterContentBurst(watch.ElapsedTicks);
            }
        }





        /// <summary>
        /// Fills the hotel network.
        /// </summary>
        /// <param name="language">The language.</param>
        /// <param name="provider">The provider.</param>
        private void FillHotelNetwork(string language, string provider)
        {
            int rootPageReferenceID = Convert.ToInt32(ConfigHelper.GetValue(ConfigKeys.RootPageReferenceID));
            string countryPageTypeID = ConfigHelper.GetValue(ConfigKeys.CountryPageTypeID);
            string cityPageTypeID = ConfigHelper.GetValue(ConfigKeys.CityPageTypeID);
            string hotelPageTypeID = ConfigHelper.GetValue(ConfigKeys.HotelPageTypeID);

            this.Network = new HotelNetwork { Language = language, Provider = provider };
            RawPage[] countryPages = ContentAccess.GetPagesWithPageTypeID(countryPageTypeID, rootPageReferenceID, language);

            List<Continent> continents = GetContinentList(countryPages);
            foreach (Continent continent in continents)
            {
                foreach (RawPage countryPage in countryPages)
                {
                    if (ContentHelper.IsPagePublished(countryPage))
                    {
                        Country country = new Country();
                        country.Id = ContentHelper.GetPropertyValue(countryPage, "CountryCode");
                        country.Name = ContentHelper.GetPropertyValue(countryPage, "PageName");
                        string continentName = ContentHelper.GetPropertyValue(countryPage, "Continent");
                        //country.APITaxRates = ContentHelper.GetPropertyValue(countryPage, "APITaxRates");

                        string strAPITaxRates = ContentHelper.GetPropertyValue(countryPage, "APITaxRates");
                        double apitaxRates;
                        Double.TryParse(strAPITaxRates, out apitaxRates);
                        country.APITaxRates = apitaxRates;

                        int countryPageReferenceID = Convert.ToInt32(ContentHelper.GetPropertyValue(countryPage, "PageLink"));
                        if (continent.Name.Equals(continentName, StringComparison.InvariantCultureIgnoreCase))
                        {
                            RawPage[] citypages = ContentAccess.GetPagesWithPageTypeID(cityPageTypeID, countryPageReferenceID, language);
                            foreach (RawPage cityPage in citypages)
                            {
                                if (ContentHelper.IsPagePublished(cityPage))
                                {
                                    City city = new City();
                                    city.Id = ContentHelper.GetPropertyValue(cityPage, "OperaID");
                                    city.Name = ContentHelper.GetPropertyValue(cityPage, "PageName");
                                    int cityPageReferenceID = Convert.ToInt32(ContentHelper.GetPropertyValue(cityPage, "PageLink"));
                                    RawPage[] hotelPages = ContentAccess.GetPagesWithPageTypeID(hotelPageTypeID, cityPageReferenceID, language);
                                    foreach (RawPage hotelPage in hotelPages)
                                    {
                                        if (ContentHelper.IsPagePublished(hotelPage))
                                        {
                                            Hotel hotel = FillHotel(hotelPage);
                                            hotel.APITaxRates = country.APITaxRates;
                                            city.Hotels.Add(hotel);
                                        }
                                    }

                                    country.Cities.Add(city);
                                }
                            }
                        }

                        continent.Countries.Add(country);
                    }
                }

                this.Network.Continents.Add(continent);
            }
        }

        /// <summary>
        /// This method populates business entity - hotel from the CMS hotel PageData.
        /// </summary>
        /// <param name="hotelPage">The hotel page.</param>
        /// <returns>Businness entity Hotel with the properties filled from pagedata object</returns>
        /// <remarks></remarks>
        private static Hotel FillHotel(RawPage hotelPage)
        {
            Hotel hotel = new Hotel();
            hotel.Id = ContentHelper.GetPropertyValue(hotelPage, "OperaID");
            hotel.Name = ContentHelper.GetPropertyValue(hotelPage, "Heading");
            hotel.PostalCity = ContentHelper.GetPropertyValue(hotelPage, "HotelCategory");
            hotel.PropertyType = ContentHelper.GetPropertyValue(hotelPage, "HotelCategory");
            if (!string.IsNullOrEmpty(ContentHelper.GetPropertyValue(hotelPage, "AvailableInBooking")))
            {
                hotel.IsBookable = Convert.ToBoolean(ContentHelper.GetPropertyValue(hotelPage, "AvailableInBooking"));
            }

            hotel.PostalCity = ContentHelper.GetPropertyValue(hotelPage, "PostalCity");
            return hotel;
        }
    }
}
