// <copyright file="ThreadPoolImpl.cs" company="Scandic Hotels">
// Copyright � Scandic
// </copyright>

namespace Scandic.Services.BusinessServices
{
    using System;
    using System.Collections;
    using System.Threading;

    /// <summary>
    /// ThreadPoolImpl class
    /// </summary>
    public sealed class ThreadPoolImpl
    {
       #region SpawnThreadPool

        /// <summary>
        /// Method to Spawn Thread Pool
        /// </summary>
        /// <param name="callBackMethod">waitcallBack Object</param>
        /// <param name="parameters">Hastable Object</param>
        public static void SpawnThreadPool(WaitCallback callBackMethod, Hashtable parameters)
        {
            ThreadPool.QueueUserWorkItem(callBackMethod, parameters);
        }
        #endregion SpawnThreadPool
    }
}