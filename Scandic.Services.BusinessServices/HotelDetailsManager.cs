﻿// <copyright file="HotelDetailsManager.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.BusinessServices
{
    #region References

    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Threading.Tasks;
    using System.Xml;    
    using Scandic.Services.BusinessEntity;
    using Scandic.Services.Framework;
    using Scandic.Services.ServiceAgents;
    using Scandic.Services.ServiceAgents.Cms.CmsProxy;
    using Scandic.Services.ServiceAgents.CmsEntity;

    #endregion

    /// <summary>
    /// Manages the HotelDetailsCollection data
    /// </summary>    
    public class HotelDetailsManager : EntityManagerBase
    {
        /// <summary>
        /// Lock object
        /// </summary>
        private static Object lockObject = new Object();

        /// <summary>
        /// Holds the count of the Images configured
        /// </summary>
        private const int ImageCount = 10;

        /// <summary>
        /// Initializes a new instance of the HotelDetailsManager class
        /// </summary>
        /// <param name="language">The language.</param>
        /// <param name="provider">The provider.</param>      
        public HotelDetailsManager(string language, string provider)
        {
            this.HotelCollection = new HotelDetailsList { Language = language, Provider = provider };
            this.Language = language;
            this.CacheKey = string.Format(
                CultureInfo.InvariantCulture,
                "{0}-{1}-{2}",
                "HotelDetailsManager",
                this.HotelCollection.Provider,
                this.HotelCollection.Language);
            this.Initialize();
        }

        /// <summary>
        /// Gets the hotelDetails collection to be populated and managed
        /// </summary>       
        public HotelDetailsList HotelCollection { get; private set; }

        /// <summary>
        /// Helper method to fill the HotelDetails Collection
        /// </summary>
        public override void SetManagedEntity()
        {
            bool refreshEntity = !(this.IsCacheValid);
            if (!refreshEntity)
            {
                this.HotelCollection = this.CachedValue as HotelDetailsList;
            }
            
            refreshEntity = refreshEntity || this.HotelCollection == null;

            // If an entity needs to be refreshed i.e.
            // it is not in cache but needs to be picked
            // from the offline file.            
            if (refreshEntity)
            { 
                // There can be more than one thread trying to refresh
                // So, allow only one thread to pick data from file.
                lock (lockObject)
                {
                    if (this.CachedValue == null)
                    {
                        var entity = this.RefreshEntity<HotelDetailsList>();
                        if (!(entity == default(HotelDetailsList)))
                        {
                            this.HotelCollection = entity;
                        }

                        if (this.IsCached)
                        {
                            this.CachedValue = this.HotelCollection;
                        }
                    }
                    else
                    {
                        this.HotelCollection = this.CachedValue as HotelDetailsList;
                        LogHelper.LogInfo("HotelDetailsList already loaded by another thread", LogCategory.Content);                    
                    }
                }
            }
        }

        #region Helpers
        /// <summary>
        /// Gets the value of a node
        /// </summary>
        /// <param name="parent">parent node in which to search</param>
        /// <param name="node">node name to search</param>
        /// <param name="numeric">indicates if the node is expected to be numeric default : false</param>
        /// <returns>cleaned up value of the node</returns>
        private static string GetNodeValue(XmlNode parent, string node, bool numeric = false)
        {
            if (parent.SelectSingleNode(node) != null)
            {
                return parent.SelectSingleNode(node).InnerText;
            }
            else
            {
                if (numeric)
                {
                    return "0";
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// Fills the images.
        /// </summary>
        /// <param name="hotel">The hotel.</param>
        /// <param name="hotelPage">The hotel page.</param>
        private static void FillImages(HotelDetails hotel, RawPage hotelPage)
        {
            // hotel.Images = new List<Image>();
            string path;
            string title;

            // Populate GeneralImages
            for (int i = 1; i <= ImageCount; i++)
            {
                path = "GeneralImage" + i.ToString();
                title = "GeneralImageTitle" + i.ToString();
                Image image = new Image
                {
                    ImageUrl = ContentHelper.GetPropertyValue(hotelPage, path),
                    Title = ContentHelper.GetPropertyValue(hotelPage, title),
                    IsMainImage = "false"
                };

                if ((!string.IsNullOrEmpty(image.ImageUrl)) || (!string.IsNullOrEmpty(image.Title)))
                {
                    hotel.Images.Add(image);
                }
            }

            // Populate RoomImages
            for (int i = 1; i <= ImageCount; i++)
            {
                path = "RoomImage" + i.ToString();
                title = "RoomImageTitle" + i.ToString();
                Image image = new Image
                {
                    ImageUrl = ContentHelper.GetPropertyValue(hotelPage, path),
                    Title = ContentHelper.GetPropertyValue(hotelPage, title),
                    IsMainImage = "false"
                };

                if ((!string.IsNullOrEmpty(image.ImageUrl)) || (!string.IsNullOrEmpty(image.Title)))
                {
                    hotel.Images.Add(image);
                }
            }

            // Populate LeisureImages
            for (int i = 1; i <= ImageCount; i++)
            {
                path = "LeisureImage" + i.ToString();
                title = "LeisureImageTitle" + i.ToString();
                Image image = new Image
                {
                    ImageUrl = ContentHelper.GetPropertyValue(hotelPage, path),
                    Title = ContentHelper.GetPropertyValue(hotelPage, title),
                    IsMainImage = "false"
                };

                if ((!string.IsNullOrEmpty(image.ImageUrl)) || (!string.IsNullOrEmpty(image.Title)))
                {
                    hotel.Images.Add(image);
                }
            }

            // Populate RetuarantBarImages
            for (int i = 1; i <= ImageCount; i++)
            {
                path = "RestBarImage" + i.ToString();
                title = "RestaurantBarImageTitle" + i.ToString();
                Image image = new Image
                {
                    ImageUrl = ContentHelper.GetPropertyValue(hotelPage, path),
                    Title = ContentHelper.GetPropertyValue(hotelPage, title),
                    IsMainImage = "false"
                };

                if ((!string.IsNullOrEmpty(image.ImageUrl)) || (!string.IsNullOrEmpty(image.Title)))
                {
                    hotel.Images.Add(image);
                }
            }

            if (hotel.Images.Count > 0)
            {
                hotel.Images[0].IsMainImage = "true";
            }
        }

        /// <summary>
        /// Fills the room facilties.
        /// </summary>
        /// <param name="hotel">The hotel.</param>
        /// <param name="hotelPage">The hotel page.</param>
        /// <param name="language">The language.</param>
        private static void FillRoomFacilties(HotelDetails hotel, RawPage hotelPage, string language)
        {
            int hotelReferenceID = Convert.ToInt32(ContentHelper.GetPropertyValue(hotelPage, "PageLink"));
            string roomFacilitiesPageTypeID = ConfigHelper.GetValue(ConfigKeys.HotelRoomDescriptionPageTypeID);
            RawPage[] roomFacilitiesPages = ContentAccess.GetPagesWithPageTypeID(roomFacilitiesPageTypeID, hotelReferenceID, language);
            foreach (RawPage roomFacilitiesPage in roomFacilitiesPages)
            {
                if (ContentHelper.IsPagePublished(roomFacilitiesPage))
                {
                    RoomFacilities roomFacilities = new RoomFacilities();
                    roomFacilities.Name = ContentHelper.GetPropertyValue(roomFacilitiesPage, "PageName");

                    // Abhishek:artf1250482 : Fallback description isn't sent start
                    // Get the HotelSpecific RoomDescription in a Locale.
                    string roomDescription = ContentHelper.GetPropertyValueNotMasterValue(roomFacilitiesPage, "RoomDescription");

                    // If RoomDescription is Null then get it from RoomCategory in StructuredNode
                    if (string.IsNullOrEmpty(roomDescription))
                    {
                        RoomCategory roomCategory = new RoomTypeManager(language).RoomTypes.RoomCategoryCollection.Find(delegate(RoomCategory room)
                                {
                                    return room.RoomCategoryName.Trim().ToLower().Equals(roomFacilities.Name.Trim().ToLower());
                                });

                        roomDescription = roomCategory != null ?
                            roomCategory.RoomCategoryDescription : string.Empty;                            
                    }
                    // or Get the MaterValue in English.
                    if (string.IsNullOrEmpty(roomDescription))
                    {
                        roomDescription = ContentHelper.GetPropertyValue(roomFacilitiesPage, "RoomDescription");
                    }
                    //Abhishek:artf1250482 : Fallback description isn't sent end
                    roomFacilities.RoomCategoryPageReferenceId = ContentHelper.GetPropertyValue(roomFacilitiesPage, "RoomCategory");
                    roomFacilities.Description = roomDescription;
                    roomFacilities.Image.ImageUrl = ContentHelper.GetPropertyValue(roomFacilitiesPage, "RoomImage");
                    hotel.RoomFacilitiesList.Add(roomFacilities);
                }
            }
        }

        /// <summary>
        /// Fills the alternative hotels.
        /// </summary>
        /// <param name="hotel">The hotel.</param>
        /// <param name="hotelPage">The hotel page.</param>
        /// <param name="language">The language.</param>
        private static void FillAlternativeHotels(HotelDetails hotel, RawPage hotelPage, string language)
        {
            int hotelReferenceID = Convert.ToInt32(ContentHelper.GetPropertyValue(hotelPage, "PageLink"));
            string alternativeHotelPageTypeID = ConfigHelper.GetValue(ConfigKeys.AlternativeHotelPageTypeID);
            RawPage[] alternativeHotelPages = ContentAccess.GetPagesWithPageTypeID(alternativeHotelPageTypeID, hotelReferenceID, language);
            foreach (RawPage alternativeHotelPage in alternativeHotelPages)
            {
                if (ContentHelper.IsPagePublished(alternativeHotelPage))
                {
                    if (!string.IsNullOrEmpty(ContentHelper.GetPropertyValue(alternativeHotelPage, "AlternativeHotel")))
                    {
                        hotel.AlternateHotels.Add(ContentHelper.GetPropertyValue(alternativeHotelPage, "AlternativeHotel"));
                    }
                }
            }
        }

        /// <summary>
        /// Fills the hotel details collection.
        /// </summary>
        public void DownloadHotelPages()
        {
            this.HotelCollection = new HotelDetailsList { Language = this.Language, Provider = this.HotelCollection.Provider };
            RawPage[] hotelPages = ContentAccess.GetPagesWithPageTypeID(
                ConfigHelper.GetValue(ConfigKeys.HotelPageTypeID), // This is the Page Type for Hotels
                Convert.ToInt32(ConfigHelper.GetValue(ConfigKeys.CountryContainerPageReferenceID)), // This is reference to the Container Page
                this.Language);
            this.ConvertPagesToEntity(hotelPages);
            Serializer.ObjectToFile(this.OfflineFile, this.HotelCollection);
        }


        /// <summary>
        /// Fills the hotel details.
        /// </summary>
        /// <param name="hotelPages">The hotel pages.</param>
        private void ConvertPagesToEntity(RawPage[] hotelPages)
        {
            Stopwatch watch = new Stopwatch();
            this.HotelCollection = new HotelDetailsList();
            watch.Start();

            if (bool.Parse(ConfigHelper.GetValue(ConfigKeys.EnableContentThreads)))
            {
                List<Exception> exceptions = null;
                Parallel.ForEach(
                    hotelPages,
                    hotelPage =>
                    {
                        try
                        {
                            if (ContentHelper.IsPagePublished(hotelPage))
                            {
                                HotelDetails hotelDetails = new HotelDetails();

                                hotelDetails.HotelId = ContentHelper.GetPropertyValue(hotelPage, "OperaID");
                                hotelDetails.HotelPageReferenceId = ContentHelper.GetPropertyValue(hotelPage, "PageLink");
                                hotelDetails.HotelName = ContentHelper.GetPropertyValue(hotelPage, "Heading");
                                hotelDetails.HotelUrl = ContentHelper.GetPropertyValue(hotelPage, "PageLinkURL");

                                hotelDetails.HotelAddress.Country = ContentHelper.GetPropertyValue(hotelPage, "Country");
                                hotelDetails.HotelAddress.MarketingCityName = ContentHelper.GetPropertyValue(hotelPage, "City");
                                hotelDetails.HotelAddress.PostalCity = ContentHelper.GetPropertyValue(hotelPage, "PostalCity");
                                hotelDetails.HotelAddress.PostalCode = ContentHelper.GetPropertyValue(hotelPage, "PostCode");
                                hotelDetails.HotelAddress.StreetAddress = ContentHelper.GetPropertyValue(hotelPage, "StreetAddress");

                                hotelDetails.HotelCoordinate.Latitude = ContentHelper.GetPropertyValue(hotelPage, "GeoY");
                                hotelDetails.HotelCoordinate.Longitude = ContentHelper.GetPropertyValue(hotelPage, "GeoX");

                                hotelDetails.HotelFax = ContentHelper.GetPropertyValue(hotelPage, "Fax");
                                hotelDetails.HotelPhone = ContentHelper.GetPropertyValue(hotelPage, "Phone");
                                hotelDetails.HotelEmail = ContentHelper.GetPropertyValue(hotelPage, "Email");

                                hotelDetails.HotelDescription.HotelFacilitiesDescription = ContentHelper.GetPropertyValue(hotelPage, "FacilitiesDescription");
                                hotelDetails.HotelDescription.HotelIntro = ContentHelper.GetPropertyValue(hotelPage, "HotelIntro");

                                hotelDetails.HotelFacilities.AirportName = ContentHelper.GetPropertyValue(hotelPage, "Airport1Name");
                                hotelDetails.HotelFacilities.DistanceToAirportInKM = ContentHelper.GetPropertyValue(hotelPage, "Airport1Distance");
                                hotelDetails.HotelFacilities.DistanceToCityCenterInKM = ContentHelper.GetPropertyValue(hotelPage, "CityCenterDistance");
                                hotelDetails.HotelFacilities.DistanceToTrainStationInKM = ContentHelper.GetPropertyValue(hotelPage, "DistanceTrain");
                                hotelDetails.HotelFacilities.EcoLabeledHotel = ContentHelper.GetPropertyValue(hotelPage, "EcoLabeled");
                                hotelDetails.HotelFacilities.Garage = ContentHelper.GetPropertyValue(hotelPage, "Garage");
                                hotelDetails.HotelFacilities.MeetingFacilities = ContentHelper.GetPropertyValue(hotelPage, "MeetingFacilities");
                                hotelDetails.HotelFacilities.NonsmokingRooms = ContentHelper.GetPropertyValue(hotelPage, "NonSmokingRooms");
                                hotelDetails.HotelFacilities.NumberOfRooms = ContentHelper.GetPropertyValue(hotelPage, "NoOfRooms");
                                hotelDetails.HotelFacilities.OutdoorParking = ContentHelper.GetPropertyValue(hotelPage, "OutdoorParking");
                                hotelDetails.HotelFacilities.RelaxCenter = ContentHelper.GetPropertyValue(hotelPage, "RelaxCenter");
                                hotelDetails.HotelFacilities.RestaurantOrBar = ContentHelper.GetPropertyValue(hotelPage, "RestaurantBar");
                                hotelDetails.HotelFacilities.RoomsForDisabled = ContentHelper.GetPropertyValue(hotelPage, "RoomsForDisabled");
                                hotelDetails.HotelFacilities.Shop = ContentHelper.GetPropertyValue(hotelPage, "Shop");
                                hotelDetails.HotelFacilities.WirelessInternetAccess = ContentHelper.GetPropertyValue(hotelPage, "WirelessInternet");
                                hotelDetails.HotelBookingImage.ImageUrl = ContentHelper.GetPropertyValue(hotelPage, "HotelBookingImage");
                                //hotelDetails.APITaxRates = ContentHelper.GetPropertyValue(hotelPage, "APITaxRates");
                                HotelNetwork hn = new HotelNetwork();
                                hotelDetails.APITaxRates = hn.GetVatForHotelFromCountry(hotelDetails.HotelId);
                                FillImages(hotelDetails, hotelPage);
                                FillRoomFacilties(hotelDetails, hotelPage, this.Language);
                                FillAlternativeHotels(hotelDetails, hotelPage, this.Language);
                                this.HotelCollection.Hotels.Add(hotelDetails);
                            }
                        }

                        catch (Exception ex)
                        {
                            if (exceptions == null)
                            {
                                exceptions = new List<Exception>();
                            }
                            exceptions.Add(ex);
                        }
                        finally
                        {
                            if (exceptions != null) throw new AggregateException(exceptions);
                        }
                    });
            }

            watch.Stop();
            PerformanceHelper.RegisterContentBurst(watch.ElapsedTicks);
        }
        #endregion
    }
}
