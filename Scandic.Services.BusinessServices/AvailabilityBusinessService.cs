﻿// <copyright file="AvailabilityBusinessService.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.BusinessServices
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Scandic.Services.BusinessContracts;
    using Scandic.Services.BusinessEntity;
    using Scandic.Services.Framework;
    using Scandic.Services.ServiceAgents;
    using Scandic.Services.ServiceAgents.OwsProxy.Availability;

    /// <summary>
    /// Implements methods Availability
    /// </summary>    
    public class AvailabilityBusinessService : IAvailabilityBusinessContract
    {
        #region Constants For LowestRate

        /// <summary>
        /// RoomsPerNight value
        /// </summary>
        private const int RoomsPerNight = 1;

        /// <summary>
        /// NoOfNights value
        /// </summary>
        private const int NoOfNights = 1;

        /// <summary>
        /// ChildrenOccupancyPerRoom value
        /// </summary>
        private const int ChildrenOccupancyPerRoom = 0;

        /// <summary>
        /// ChildrenPerRoom value
        /// </summary>
        private const int ChildrenPerRoom = 0;

        #endregion

        #region Constants

        /// <summary>
        /// Lower limit for childern age.
        /// </summary>
        private const int ChildernAgeLowerLimit = 0;

        /// <summary>
        /// The format to log lowest rates availability
        /// </summary>
        private const string SearchParameters = "Requestedcity: {4}\nSearchParameters: No of Adult: {0}, No of Child: {1}, No of Rooms:{2}, No of Nights: {3}\n";
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the language in which entities
        /// are to be returned, where applicable
        /// </summary>
        public string Language { get; set; }

        /// <summary>
        /// Gets or sets the provider for which entities
        /// are to be returned, where applicable
        /// </summary>
        public string Provider { get; set; }

        /// <summary>
        /// Gets or sets the partner API key.
        /// </summary>
        /// <value>
        /// The partner API key.
        /// </value>
        public string PartnerApiKey { get; set; }

        #endregion

        #region Service Methods
        /// <summary>
        /// Get the rooms and rates available for booking in a hotel
        /// </summary>
        /// <param name="hotelId">hotelId value</param>
        /// <param name="arrivalDate">arrivalDate value</param>
        /// <param name="departureDate">departureDate value</param>
        /// <param name="adultCounts">adultCounts value</param>
        /// <param name="childCounts">childCounts value</param>
        /// <param name="childrenAges">childrenAges value</param>
        /// <param name="bookingCode">bookingCode value</param>        
        /// <returns>
        /// rooms and rates available for booking in multiple hotels
        /// </returns>
        public HotelRoomsAndRates GetMultipleRoomAndRates(string[] hotelId, DateTime arrivalDate, DateTime departureDate, int[] adultCounts, int[] childCounts, int[,] childrenAges, string bookingCode, string id)
        {
            HotelSearchEntity hotelSearchEntity = this.CreateHotelSearchRequest(arrivalDate, departureDate, adultCounts, childCounts, childrenAges, bookingCode);
            if (hotelId.Length > 0)
            {
                for (int i = 0; i < hotelId.Length; i++)
                {
                    if (!string.IsNullOrEmpty(hotelId[i]))
                    {
                        if (HotelNetworkHelper.IsHotelValidAndBookable(hotelId[i]))
                        {
                            hotelSearchEntity.SelectedHotelCode.Add(hotelId[i]);
                        }
                    }
                }
                return new AvailabilityManager(this.Language, this.Provider, this.PartnerApiKey).GetMultipleRoomsandRates(hotelSearchEntity, id);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Get the rooms and rates available for booking in a hotel
        /// </summary>
        /// <param name="hotelId">hotelId value</param>
        /// <param name="arrivalDate">arrivalDate value</param>
        /// <param name="departureDate">departureDate value</param>
        /// <param name="adultCounts">adultCounts value</param>
        /// <param name="childCounts">childCounts value</param>
        /// <param name="childrenAges">childrenAges value</param>
        /// <param name="bookingCode">bookingCode value</param>        
        /// <returns>
        /// rooms and rates available for booking in a hotel
        /// </returns>
        public RoomsAndRates GetRoomAndRates(string hotelId, DateTime arrivalDate, DateTime departureDate, int[] adultCounts, int[] childCounts, int[,] childrenAges, string bookingCode, string id)
        {
            HotelSearchEntity hotelSearchEntity = this.CreateHotelSearchRequest(arrivalDate, departureDate, adultCounts, childCounts, childrenAges, bookingCode);
            hotelSearchEntity.SelectedHotelCode.Add(hotelId);
            if (HotelNetworkHelper.IsHotelValidAndBookable(hotelId))
            {
                hotelSearchEntity.SelectedHotelCode.Add(hotelId);
                return new AvailabilityManager(this.Language, this.Provider, this.PartnerApiKey).GetRoomsandRates(hotelSearchEntity, id);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Gets the availability for city.
        /// </summary>
        /// <param name="city">city value</param>
        /// <param name="arrivalDate">arrivalDate value</param>
        /// <param name="departureDate">departureDate value</param>
        /// <param name="adultCounts">adultCounts value</param>
        /// <param name="childCounts">childCounts value</param>
        /// <param name="childrenAges">childrenAges value</param>
        /// <param name="bookingCode">bookingCode value</param>
        /// <param name="id">The id of the request - used for logging</param>
        /// <returns>
        /// List of Available hotels in a city
        /// </returns>
        public AvailableHotelDetailsList GetAvailabilityForCity(string city, DateTime arrivalDate, DateTime departureDate, int[] adultCounts, int[] childCounts, int[,] childrenAges, string bookingCode, string id)
        {
            HotelSearchEntity hotelSearch = this.CreateHotelSearchRequest(arrivalDate, departureDate, adultCounts, childCounts, childrenAges, bookingCode);
            hotelSearch.HotelCityId = city;
            List<Hotel> hotelList = HotelNetworkHelper.BookableHotelsInCity(city).Distinct(new HotelComparer()).ToList();
            hotelSearch.SelectedHotelCode.Clear();
            foreach (Hotel hotel in hotelList)
            {
                hotelSearch.SelectedHotelCode.Add(hotel.Id);
            }

            AvailableHotelDetailsList hotelDetailsList = new AvailabilityManager(this.Language, this.Provider, this.PartnerApiKey)
               .GetAvailabilityMultipleHotels(hotelSearch, LogCategory.OWSForCity, id);
            hotelDetailsList.CityId = city;
            this.SetDeepLinkUrls(hotelDetailsList, AvailabilityApis.AvailabilityForCity, hotelSearch);
            return hotelDetailsList;
        }

        /// <summary>
        /// Gets the availability for certain hotels.
        /// </summary>
        /// <param name="hotelId">hotelId value</param>
        /// <param name="arrivalDate">arrivalDate value</param>
        /// <param name="departureDate">departureDate value</param>
        /// <param name="adultCounts">adultCounts value</param>
        /// <param name="childCounts">childCounts value</param>
        /// <param name="childrenAges">childrenAges value</param>
        /// <param name="bookingCode">bookingCode value</param>
        /// <param name="id">The id of the request - used for logging</param>
        /// <returns>
        /// List of availabilty of certain hotels
        /// </returns>
        public AvailableHotelDetailsList GetAvailabilityForCertainHotels(string[] hotelId, DateTime arrivalDate, DateTime departureDate, int[] adultCounts, int[] childCounts, int[,] childrenAges, string bookingCode, string id)
        {
            AvailableHotelDetailsList newList = default(AvailableHotelDetailsList);
            HotelSearchEntity hotelSearch = this.CreateHotelSearchRequest(arrivalDate, departureDate, adultCounts, childCounts, childrenAges, bookingCode);
            for (int i = 0; i < hotelId.Length; i++)
            {
                if (!string.IsNullOrEmpty(hotelId[i]))
                {
                    hotelSearch.SelectedHotelCode.Add(hotelId[i]);
                }
            }

            newList = new AvailabilityManager(this.Language, this.Provider, this.PartnerApiKey).GetAvailabilityMultipleHotels(hotelSearch, LogCategory.OWSForCertainHotels, id);

            if (newList.AvailableHotels.Count.Equals(0) && hotelId.Length.Equals(1) && string.IsNullOrEmpty(hotelSearch.CampaignCode))
            {
                HotelDetailsList hotels = new HotelDetailsManager(this.Language, this.Provider).HotelCollection;

                string searchedHotelName = (hotels.Hotels.Count > 0) ? hotels.Hotels.Single(htl => htl.HotelId == hotelId[0]).HotelName : string.Empty;

                List<string> alternativeHotels = HotelNetworkHelper.GetAlternativeHotels(hotelId[0]) as List<string>;
                if (alternativeHotels.Count > 0)
                {
                    hotelSearch.SelectedHotelCode.Clear();
                    for (int i = 0; i < alternativeHotels.Count; i++)
                    {
                        var hotelDetailObj = hotels.Hotels.SingleOrDefault(htl => htl.HotelPageReferenceId == alternativeHotels[i]);

                        if (hotelDetailObj != null)
                        {
                            string operaId = hotelDetailObj.HotelId;
                            hotelSearch.SelectedHotelCode.Add(operaId);
                        }
                    }

                    newList = new AvailabilityManager(this.Language, this.Provider, this.PartnerApiKey).GetAvailabilityMultipleHotels(hotelSearch, LogCategory.OWSForCertainHotels, id);
                    newList.MessageOnSingleHotelSearchNotAvailable = searchedHotelName;
                }
            }

            if (newList != null && newList.AvailableHotels.Count > 0)
            {
                this.SetDeepLinkUrls(newList, AvailabilityApis.AvailabilityForCertainHotels, hotelSearch);
            }

            return newList;
        }

        /// <summary>
        /// Gets the lowest rate for city.
        /// </summary>
        /// <param name="city">The city search.</param>
        /// <param name="id">The id of the request</param>
        /// <returns>
        /// Available Hotel detail list
        /// </returns>
        public AvailableHotelDetailsList GetLowestRateForCity(string city, string id)
        {

            IList<HotelSearchEntity> hotelSearchRequest = this.CreateHotelSerachRequestFromSettings(city);
            List<Hotel> hotelList = HotelNetworkHelper.BookableHotelsInCity(city).Distinct(new HotelComparer()).ToList();
            string logMessage = string.Empty;
            StringBuilder hotelsAvailabilityData = new StringBuilder();

            List<AvailableHotelDetails> combinedHotels = new List<AvailableHotelDetails>();
            for (int i = 0; i < hotelSearchRequest.Count; i++)
            {
                HotelSearchEntity hotelSearch = hotelSearchRequest[i];
                hotelSearch.SelectedHotelCode.Clear();
                foreach (Hotel hotel in hotelList)
                {
                    hotelSearch.SelectedHotelCode.Add(hotel.Id);
                }

                AvailableHotelDetailsList hotelDetailsList = new AvailabilityManager(this.Language, this.Provider, this.PartnerApiKey).GetAvailabilityMultipleHotels(hotelSearch, LogCategory.OWSLowestRate, id);
                combinedHotels = combinedHotels.Concat(hotelDetailsList.AvailableHotels).ToList<AvailableHotelDetails>();
                if (Convert.ToBoolean(ConfigHelper.GetValue(ConfigKeys.LogLowestRate)))
                {
                    List<LowestRateLogDetails> logData = (from availableHotels in combinedHotels
                                                          from availableHotelRoomDetails in availableHotels.AvailableHotelRoomDetails
                                                          from roomRate in availableHotelRoomDetails.RoomRates
                                                          select (new LowestRateLogDetails()
                                                          {
                                                              HotelName = availableHotels.HotelName,
                                                              RateName = roomRate.FromRate.RateName,
                                                              RoomType = roomRate.RoomType,
                                                              Currency = roomRate.FromRate.PricePerNightCurrencyCode,
                                                              PricePerNight = roomRate.FromRate.PricePerNight.ToString()
                                                          })).ToList();

                    foreach (LowestRateLogDetails rateData in logData)
                    {
                        hotelsAvailabilityData.Append(string.Format("{0} | {1} \n", hotelSearch.ArrivalDate.ToShortDateString(), rateData.ToString()));
                    }
                }
            }

            if (Convert.ToBoolean(ConfigHelper.GetValue(ConfigKeys.LogLowestRate)))
            {
                logMessage = string.Format(
                    SearchParameters,
                    hotelSearchRequest[0].ListRooms[0].AdultsPerRoom,
                    hotelSearchRequest[0].ListRooms[0].ChildrenPerRoom,
                    hotelSearchRequest[0].RoomsPerNight,
                    hotelSearchRequest[0].NoOfNights,
                    hotelSearchRequest[0].HotelCityId);

                LogHelper.LogInfo(logMessage + hotelsAvailabilityData, LogCategory.LowestRate);
            }

            List<AvailableHotelDetails> lows = default(List<AvailableHotelDetails>);
            if (combinedHotels.Count > 0)
            {
                var allLowestRates = from rates in combinedHotels
                                     group rates by rates.HotelId into hotelGroup
                                     join lowest in combinedHotels on hotelGroup.Key equals lowest.HotelId
                                     where lowest.HotelFromRate.PricePerNight == hotelGroup.Min(r => r.HotelFromRate.PricePerNight)
                                     orderby lowest.HotelFromRate.PricePerNight
                                     select lowest;
                if (allLowestRates != null)
                {
                    lows = allLowestRates.GroupBy(g => g.HotelId).Select(f => f.First()).ToList();
                }
            }

            AvailableHotelDetailsList newList = new AvailableHotelDetailsList(lows)
            {
                Continent = HotelNetworkHelper.GetContinentForCity(hotelSearchRequest[0].HotelCityId, this.Language),
                Language = this.Language,
                Provider = this.Provider,
                CityId = city
            };

            if (newList.AvailableHotels.Count > 0)
            {
                this.SetDeepLinkUrls(newList, AvailabilityApis.LowestRateForCity, hotelSearchRequest[0]);
            }

            return newList;
        }

        /// <summary>
        /// Refreshes all cached entities
        /// </summary>
        public void RefreshCache()
        {
            string[] languages = ConfigHelper.GetValue(ConfigKeys.ValidLanguages).Split(',');
            string provider = ConfigHelper.GetValue(ConfigKeys.Provider);

            foreach (string language in languages)
            {
                try
                {
                    LogHelper.LogInfo(
                    string.Format("Preloading Cache: Rates | {0} | {1}", provider, language),
                    LogCategory.Service);
                    //new RateTypeManager(language).RefreshEntity();
                    new RateTypeManager(language);
                }
                catch (Exception ex)
                {
                    LogHelper.LogException(ex, LogCategory.Service);
                }

                try
                {
                    LogHelper.LogInfo(
                    string.Format("Preloading Cache: Rooms | {0} | {1}", provider, language),
                    LogCategory.Service);
                    //new RoomTypeManager(language).RefreshEntity();
                    new RoomTypeManager(language, true);
                }
                catch (Exception ex)
                {
                    LogHelper.LogException(ex, LogCategory.Service);
                }

            }

        }
        #endregion

        #region Helpers
        /// <summary>
        /// Creates the hotel serach entity form settings.
        /// </summary>
        /// <param name="marketingCity">The marketing city.</param>
        /// <returns>list of hotel search entity</returns>
        private IList<HotelSearchEntity> CreateHotelSerachRequestFromSettings(string marketingCity)
        {
            List<HotelSearchEntity> hotelSearchEntities = new List<HotelSearchEntity>();
            int leadDays = int.Parse(ConfigHelper.GetValue(ConfigKeys.LeadTime));
            int noOfDays = int.Parse(ConfigHelper.GetValue(ConfigKeys.NoOfDays));
            string startDay = ConfigHelper.GetValue(ConfigKeys.StartDay);
            DateTime arrivalDate = this.GetNextWeekday(leadDays, startDay);
            DateTime departureDate = default(DateTime);
            HotelSearchEntity hotelSearch;
            HotelSearchRoomEntity room;
            for (int i = 0; i < noOfDays; i++)
            {
                hotelSearch = new HotelSearchEntity();
                room = new HotelSearchRoomEntity();
                departureDate = arrivalDate.AddDays(1);
                hotelSearch.HotelCityId = marketingCity;
                hotelSearch.Language = this.Language;
                hotelSearch.RoomsPerNight = RoomsPerNight; // take this from constants
                hotelSearch.ArrivalDate = arrivalDate;
                hotelSearch.DepartureDate = departureDate;
                room.AdultsPerRoom = Convert.ToInt32(ConfigHelper.GetValue(ConfigKeys.NoOfAdults));
                room.ChildrenPerRoom = ChildrenPerRoom; // take this from constants
                hotelSearch.ListRooms.Add(room);
                hotelSearchEntities.Add(hotelSearch);
                arrivalDate = arrivalDate.AddDays(1);
            }

            return hotelSearchEntities;
        }

        /// <summary>
        /// Creates hotel search entity
        /// </summary>
        /// <param name="arrivalDate">The Arrival Date</param>
        /// <param name="departureDate"> The Departure Date</param>
        /// <param name="adultCount">The adult count.</param>
        /// <param name="childCount">The child count.</param>
        /// <param name="childAges">The child ages.</param>        
        /// <param name="bookingCode">The booking code</param>
        /// <returns>Detailed information on hotel search</returns>       
        private HotelSearchEntity CreateHotelSearchRequest(DateTime arrivalDate, DateTime departureDate, int[] adultCount, int[] childCount, int[,] childAges, string bookingCode)
        {
            HotelSearchEntity hotelSearchEntity = new HotelSearchEntity();
            HotelSearchRoomEntity room = default(HotelSearchRoomEntity);
            ChildEntity child = default(ChildEntity);
            hotelSearchEntity.ArrivalDate = arrivalDate;
            hotelSearchEntity.DepartureDate = departureDate;
            hotelSearchEntity.CampaignCode = bookingCode;
            hotelSearchEntity.Language = this.Language;
            hotelSearchEntity.HotelCityId = string.Empty;
            //// To check for block code.
            if (bookingCode != null && bookingCode.ToLower().StartsWith("b"))
            {
                hotelSearchEntity.SearchingType = SearchType.CORPORATE;
                hotelSearchEntity.QualifyingType = AppConstants.BLOCKCODEQUALIFYINGTYPE;
            }

            for (int i = 0; i < adultCount.Length; i++)
            {
                room = new HotelSearchRoomEntity();
                if (adultCount[i] != 0)
                {
                    room.AdultsPerRoom = adultCount[i];
                    room.ChildrenPerRoom = childCount[i];

                    // Since its upperbound changing the condition to <=
                    for (int j = 0; j <= childAges.GetUpperBound(1); j++)
                    {
                        child = new ChildEntity();
                        if (childAges[i, j] >= ChildernAgeLowerLimit)
                        {
                            child.Age = childAges[i, j];
                            room.Listchildern.Add(child);
                        }
                    }

                    hotelSearchEntity.ListRooms.Add(room);
                }
            }

            hotelSearchEntity.RoomsPerNight = hotelSearchEntity.ListRooms.Count;
            return hotelSearchEntity;
        }

        /// <summary>
        /// Gets the date of a given weekday, skipping lead time
        /// </summary>
        /// <param name="lead">Number of days to skip</param>
        /// <param name="day">Weekday to look for</param>
        /// <returns>Date of a given weekday, skipping lead time</returns>
        private DateTime GetNextWeekday(int lead, string day)
        {
            DateTime today = DateTime.Now;
            for (int i = lead; i < lead + 7; i++)
            {
                if (today.AddDays(i).DayOfWeek.ToString().Equals(day))
                {
                    return today.AddDays(i);
                }
            }

            return default(DateTime);
        }

        /// <summary>
        /// Method to Set Deeplink Url
        /// </summary>
        /// <param name="hotelList">hotelList value</param>
        /// <param name="apiName">apiName value</param>
        /// <param name="hotelSearch">Hotel Search</param>
        private void SetDeepLinkUrls(AvailableHotelDetailsList hotelList, AvailabilityApis apiName, HotelSearchEntity hotelSearch)
        {
            switch (apiName)
            {
                case AvailabilityApis.AvailabilityForCertainHotels:
                    if (hotelList.AvailableHotels.Count > 0)
                    {
                        foreach (AvailableHotelDetails h in hotelList.AvailableHotels)
                        {
                            if (h.IsHotelAvailable)
                            {
                                h.HotelFromRate.DeepLinkUrlSelectRatePage = DeepLinkHelper.GetDeepLinkUrl(hotelSearch, DeepLinkRedirectPage.SelectRateRegular, h.HotelId, this.PartnerApiKey);
                                h.Deeplinkurlhotellangingpage = DeepLinkHelper.GetDeepLinkUrl(hotelSearch, DeepLinkRedirectPage.HotelLandingPage, h.HotelId, this.PartnerApiKey);
                            }
                        }
                    }

                    break;
                case AvailabilityApis.AvailabilityForCity:
                    {
                        if (hotelList.AvailableHotels.Count > 0)
                        {
                            foreach (AvailableHotelDetails h in hotelList.AvailableHotels)
                            {
                                if (h.IsHotelAvailable)
                                {
                                    h.HotelFromRate.DeepLinkUrlSelectRatePage = DeepLinkHelper.GetDeepLinkUrl(hotelSearch, DeepLinkRedirectPage.SelectRateRegular, h.HotelId, this.PartnerApiKey);
                                    h.Deeplinkurlhotellangingpage = DeepLinkHelper.GetDeepLinkUrl(hotelSearch, DeepLinkRedirectPage.HotelLandingPage, h.HotelId, this.PartnerApiKey);
                                }
                            }
                        }

                        hotelList.DeeplinkUrlHotelList = DeepLinkHelper.GetDeepLinkUrl(hotelSearch, DeepLinkRedirectPage.SelectHotel, hotelSearch.HotelCityId, this.PartnerApiKey);
                    }

                    break;
                case AvailabilityApis.LowestRateForCity:
                    {
                        if (hotelList.AvailableHotels.Count > 0)
                        {
                            foreach (AvailableHotelDetails h in hotelList.AvailableHotels)
                            {
                                if (h.IsHotelAvailable)
                                {
                                    h.Deeplinkurlhotellangingpage = DeepLinkHelper.GetDeeplinkUrlForLowestRates(hotelSearch, h.HotelId, DeepLinkRedirectPage.HotelLandingPage, this.PartnerApiKey);
                                }
                            }
                        }

                        hotelList.DeepLinkUrlFindHotelPage = DeepLinkHelper.GetDeeplinkUrlForLowestRates(hotelSearch, string.Empty, DeepLinkRedirectPage.FindHotelPage, this.PartnerApiKey);
                    }

                    break;
            }
        }
        #endregion
    }
}
