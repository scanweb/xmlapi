﻿// <copyright file="RoomTypeManager.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.BusinessServices
{
    #region References

    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Threading.Tasks;
    using Scandic.Services.BusinessEntity;
    using Scandic.Services.Framework;
    using Scandic.Services.ServiceAgents;
    using Scandic.Services.ServiceAgents.Cms.CmsProxy;
    using Scandic.Services.ServiceAgents.CmsEntity; 
    
    #endregion

    /// <summary>
    /// Manages the RoomType and RoomCategory data
    /// </summary>
    public class RoomTypeManager : EntityManagerBase
    {
        /// <summary>
        /// Lock object
        /// </summary>
        private static Object lockObject = new Object();

        /// <summary>
        /// Initializes a new instance of the <see cref="RoomTypeManager"/> class.
        /// </summary>
        /// <param name="language">The language.</param>
        public RoomTypeManager(string language, bool forceRefresh = false)
        {
            this.RoomTypes = new RoomTypes { Language = language };
            this.Language = language;
            this.CacheKey = string.Format(
                CultureInfo.InvariantCulture,
                "{0}-{1}",
                "RoomTypeManager",
                this.RoomTypes.Language);
            this.Initialize();
        }

        /// <summary>
        /// Gets or sets the room network.
        /// </summary>
        /// <value>
        /// The room network.
        /// </value>
        public RoomTypes RoomTypes { get; set; }  

        /// <summary>
        /// Gets the room name by room code.
        /// </summary>
        /// <param name="roomCode">The roomcode.</param>
        /// <returns>roomCategory of the room code passed in</returns>
        public RoomCategory GetRoomCategoryByRoomCode(string roomCode)
        {
            string roomCategoryName = this.RoomTypes.RoomTypeMap[roomCode];
            if (!string.IsNullOrEmpty(roomCategoryName))
            {
                return this.RoomTypes.RoomCategoryCollection.Find(delegate(RoomCategory room)
                {
                    return room.RoomCategoryName.Equals(roomCategoryName);
                });
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Determines whether [is room plan code in CMS] [the specified roomCode].
        /// </summary>
        /// <param name="roomCode">room Code for which the Room category has to be retrieved</param>
        /// <returns>boolean indicating whether the specified roomCode is configured in CMS</returns>
        public bool IsRoomCodeInCMS(string roomCode)
        {
            bool isInCMS = false;
            string roomCategoryName = this.RoomTypes.RoomTypeMap[roomCode];
            if (!string.IsNullOrEmpty(roomCategoryName))
            {
                isInCMS = true;
            }

            return isInCMS;
        }

        /// <summary>
        /// Fills the room type map.
        /// </summary>
        public override void SetManagedEntity()
        {
            bool refreshEntity = !(this.IsCacheValid);

            if (!refreshEntity)
            {
                this.RoomTypes = this.CachedValue as RoomTypes;                
            }

            refreshEntity = refreshEntity || this.RoomTypes == null;

            // If an entity needs to be refreshed i.e.
            // it is not in cache but needs to be picked
            // from the offline file.            
            if (refreshEntity)
            { 
                // There can be more than one thread trying to refresh
                // So, allow only one thread to pick data from file.
                lock (lockObject)
                {
                    if (this.CachedValue == null)
                    {
                        var entity = this.RefreshEntity<RoomTypes>();
                        if (!(entity == default(RoomTypes)))
                        {
                            this.RoomTypes = entity;
                        }

                        if (this.IsCached)
                        {
                            this.CachedValue = this.RoomTypes;
                        }
                    }
                    else
                    {
                        this.RoomTypes = this.CachedValue as RoomTypes;
                        LogHelper.LogInfo("RoomTypes already loaded by another thread", LogCategory.Content);
                    }
                }
            }
            
        }

        /// <summary>
        /// Fills the room type details.
        /// </summary>
        public void DownloadRoomTypeDetails()
        {
            Stopwatch watch = new Stopwatch();
            this.RoomTypes = new RoomTypes { Language = this.Language };
            string roomCategoryPageTypeID = ConfigHelper.GetValue(ConfigKeys.RoomCategoryPageTypeID);
            string roomTypePageTypeID = ConfigHelper.GetValue(ConfigKeys.RoomTypePageTypeID);
            int roomContainerPageReferenceID = Convert.ToInt32(ConfigHelper.GetValue(ConfigKeys.RoomContainerID));

            // Fetch all room Categories configured in CMS
            watch.Start();
            RawPage[] roomCategoryPages = ContentAccess.GetPagesWithPageTypeID(roomCategoryPageTypeID, roomContainerPageReferenceID, this.RoomTypes.Language);
            if (bool.Parse(ConfigHelper.GetValue(ConfigKeys.EnableContentThreads)))
            {
                List<Exception> exceptions = null;
                Parallel.ForEach(
                    roomCategoryPages, 
                    rawPage =>
                {
                    try
                    {
                        if (ContentHelper.IsPagePublished(rawPage))
                        {
                            RoomCategory roomCategory = new RoomCategory();
                            int pageReferenceID = Convert.ToInt32(ContentHelper.GetPropertyValue(rawPage, "PageLink"));
                            string roomCategoryName = ContentHelper.GetPropertyValue(rawPage, "PageName");
                            roomCategory.RoomCategoryName = roomCategoryName;
                            roomCategory.RoomCategoryId = ContentHelper.GetPropertyValue(rawPage, "RoomCategoryID");
                            roomCategory.PageReferenceId = pageReferenceID.ToString();
                            roomCategory.RoomCategoryDescription = ContentHelper.GetPropertyValue(rawPage, "RoomCategoryDescription");
                            if (!this.RoomTypes.RoomCategoryCollection.Contains(roomCategory))
                            {
                                this.RoomTypes.RoomCategoryCollection.Add(roomCategory);
                            }
                            // Fill a collection with the roomcodes configured in CMS and its corresponding roomcategory.                       
                            RawPage[] roomTypePages = ContentAccess.GetPagesWithPageTypeID(roomTypePageTypeID, pageReferenceID, this.RoomTypes.Language);
                            Parallel.ForEach(
                                roomTypePages,
                                roomTypePage =>
                                {
                                    try
                                    {
                                        if (ContentHelper.IsPagePublished(roomTypePage))
                                        {
                                            string roomCode = ContentHelper.GetPropertyValue(roomTypePage, "PageName");
                                            if (!string.IsNullOrEmpty(roomCode) && !string.IsNullOrEmpty(roomCategoryName) && !this.RoomTypes.RoomTypeMap.ContainsKey(roomCode))
                                            {
                                                this.RoomTypes.RoomTypeMap.Add(roomCode, roomCategoryName);
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        if (exceptions == null)
                                        {
                                            exceptions = new List<Exception>();
                                        }
                                        exceptions.Add(ex);
                                    }
                                    finally
                                    {
                                        if (exceptions != null) throw new AggregateException(exceptions);
                                    }
                                });
                        }
                    }
                    catch (Exception ex)
                    {
                        if (exceptions == null)
                        {
                            exceptions = new List<Exception>();
                        }
                        exceptions.Add(ex);
                    }
                    finally
                    {
                        if (exceptions != null) throw new AggregateException(exceptions);
                    }
                });

                Serializer.ObjectToFile(this.OfflineFile, this.RoomTypes);
            }

            watch.Stop();
            PerformanceHelper.RegisterContentBurst(watch.ElapsedTicks);
        }
    }
}
