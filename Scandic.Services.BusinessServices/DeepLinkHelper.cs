﻿// <copyright file="DeepLinkHelper.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.BusinessServices
{
    using System;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using Scandic.Services.BusinessEntity;
    using Scandic.Services.Framework;
    using Scandic.Services.ServiceAgents;

    /// <summary>
    /// Class that holds methods to generate Deeplink Url.
    /// </summary>
    public class DeepLinkHelper
    {
        /// <summary>
        /// Gets the extension for given language
        /// </summary>
        /// <param name="language">Language for which extension is to be found</param>
        /// <returns>Extension for given language</returns>
        public static string GetExtension(string language)
        {
            return ConfigHelper
                            .GetValue(ConfigKeys.LanguageDomains)
                            .Split(',')[ConfigHelper
                            .GetValue(ConfigKeys.ValidLanguages)
                            .Split(',')
                            .ToList()
                            .IndexOf(language)];
        }

        /// <summary>
        /// Gets the Deeplink URL.
        /// </summary>
        /// <param name="hotelSearch">The hotel search.</param>
        /// <param name="redirectPage">The redirect page.</param>
        /// <param name="hotelId">The hotel id.</param>
        /// <param name="partnerApiKey">The partner API key.</param>
        /// <returns>
        /// Deep link url
        /// </returns>
        public static string GetDeepLinkUrl(HotelSearchEntity hotelSearch, DeepLinkRedirectPage redirectPage, string hotelId, string partnerApiKey)
        {
            string bookingCode = string.Empty;
            string deepLinkUrl = string.Empty;
            StringBuilder urlParams = new StringBuilder();

            for (int roomIterator = 0; roomIterator < hotelSearch.RoomsPerNight; roomIterator++)
            {
                urlParams.AppendFormat(
                    CultureInfo.InvariantCulture,
                    "&CN{0}={1}&AN{0}={2}",
                    roomIterator + 1,
                    hotelSearch.ListRooms[roomIterator].ChildrenPerRoom,
                    hotelSearch.ListRooms[roomIterator].AdultsPerRoom);
            }

            urlParams.AppendFormat(CultureInfo.InvariantCulture, "{0}", GetChildrenUrl(hotelSearch));
            switch (hotelSearch.SearchingType)
            {
                case SearchType.REGULAR:
                    if (!string.IsNullOrEmpty(hotelSearch.CampaignCode))
                    {
                        bookingCode = string.Format(
                             CultureInfo.InvariantCulture,
                             "SC={0}&",
                             hotelSearch.CampaignCode.Trim());
                    }

                    break;

                case SearchType.CORPORATE:
                    if (!string.IsNullOrEmpty(hotelSearch.QualifyingType) &&
                        AppConstants.BLOCKCODEQUALIFYINGTYPE.Equals(
                        hotelSearch.QualifyingType,
                        StringComparison.InvariantCultureIgnoreCase))
                    {
                        bookingCode = string.Format(
                           CultureInfo.InvariantCulture,
                           "RB={0}&",
                           hotelSearch.CampaignCode.Trim());
                    }

                    break;
            }

            string extension = GetExtension(hotelSearch.Language);

            switch (redirectPage)
            {
                case DeepLinkRedirectPage.SelectRateRegular:
                    deepLinkUrl = string.Format(
                        CultureInfo.InvariantCulture,
                        ConfigHelper.GetValue(ConfigKeys.SelectRateLink),
                        extension,
                        bookingCode,
                        hotelId,
                        hotelSearch.RoomsPerNight,
                        hotelSearch.ArrivalDate.Day,
                        hotelSearch.ArrivalDate.Month,
                        hotelSearch.ArrivalDate.Year,
                        hotelSearch.DepartureDate.Day,
                        hotelSearch.DepartureDate.Month,
                        hotelSearch.DepartureDate.Year,
                        urlParams,
                        GetProfileId(partnerApiKey),
                        GetCmpId(partnerApiKey, hotelSearch.Language));
                    break;
                case DeepLinkRedirectPage.HotelLandingPage:
                    deepLinkUrl = string.Format(
                    CultureInfo.InvariantCulture,
                    ConfigHelper.GetValue(ConfigKeys.HotelLandingLink),
                    extension,
                    bookingCode,
                    hotelId,
                    hotelSearch.RoomsPerNight,
                    hotelSearch.ArrivalDate.Day.ToString(),
                    hotelSearch.ArrivalDate.Month.ToString(),
                    hotelSearch.ArrivalDate.Year.ToString(),
                    hotelSearch.DepartureDate.Day.ToString(),
                    hotelSearch.DepartureDate.Month.ToString(),
                    hotelSearch.DepartureDate.Year.ToString(),
                    urlParams,
                    GetProfileId(partnerApiKey),
                    GetCmpId(partnerApiKey, hotelSearch.Language));
                    break;
                case DeepLinkRedirectPage.FindHotelPage:
                    deepLinkUrl = string.Format(
                    ConfigHelper.GetValue(ConfigKeys.FindHotelLink),
                    extension,
                    Uri.EscapeUriString(hotelSearch.HotelCityId),
                    GetProfileId(partnerApiKey),
                    GetCmpId(partnerApiKey, hotelSearch.Language));
                    break;
                case DeepLinkRedirectPage.SelectHotel:
                    deepLinkUrl = string.Format(
                   CultureInfo.InvariantCulture,
                   ConfigHelper.GetValue(ConfigKeys.SelectHotelLink),
                   extension,
                   bookingCode,
                   Uri.EscapeUriString(hotelSearch.HotelCityId),
                   hotelSearch.RoomsPerNight,
                   hotelSearch.ArrivalDate.Day.ToString(),
                   hotelSearch.ArrivalDate.Month.ToString(),
                   hotelSearch.ArrivalDate.Year.ToString(),
                   hotelSearch.DepartureDate.Day.ToString(),
                   hotelSearch.DepartureDate.Month.ToString(),
                   hotelSearch.DepartureDate.Year.ToString(),
                   urlParams,
                   GetProfileId(partnerApiKey),
                   GetCmpId(partnerApiKey, hotelSearch.Language));
                    break;
                default:
                    break;
            }

            return deepLinkUrl;
        }

        /// <summary>
        /// Gets the deeplink URL for lowest rates.
        /// </summary>
        /// <param name="hotelSearchEntity">The hotel search entity.</param>
        /// <param name="hotelId">The hotel id.</param>
        /// <param name="redirectPage">The redirect page.</param>
        /// <param name="partnerApiKey">The partner API key.</param>
        /// <returns>
        /// deeplink url for lowest rates
        /// </returns>
        public static string GetDeeplinkUrlForLowestRates(HotelSearchEntity hotelSearchEntity, string hotelId, DeepLinkRedirectPage redirectPage, string partnerApiKey)
        {
            string deepLinkUrl = string.Empty;
            StringBuilder urlParams = new StringBuilder();
            string extension = GetExtension(hotelSearchEntity.Language);
            switch (redirectPage)
            {
                case DeepLinkRedirectPage.HotelLandingPage:
                    {
                        deepLinkUrl = string.Format(
                        CultureInfo.InvariantCulture,
                        ConfigHelper.GetValue(ConfigKeys.LowestRateHotelLandingLink),
                        extension,
                        hotelId,
                        GetProfileId(partnerApiKey),
                        GetCmpId(partnerApiKey, hotelSearchEntity.Language));
                        break;
                    }

                case DeepLinkRedirectPage.FindHotelPage:
                    {
                        deepLinkUrl = string.Format(
                        CultureInfo.InvariantCulture,
                        ConfigHelper.GetValue(ConfigKeys.LowestRateFindHotelLink),
                        extension,
                        Uri.EscapeUriString(hotelSearchEntity.HotelCityId),
                        GetProfileId(partnerApiKey),
                        GetCmpId(partnerApiKey, hotelSearchEntity.Language));
                        break;
                    }
            }

            return deepLinkUrl;
        }

        /// <summary>
        /// Update the collection with bed type and get the URL Params
        /// </summary>
        /// <param name="hotelSearch">Hotel Search for which URL is being built</param>
        /// <returns>The URL Params string</returns>
        private static string GetChildrenUrl(HotelSearchEntity hotelSearch)
        {
            StringBuilder urlParams = new StringBuilder();
            int childCount, roomCount;
            foreach (HotelSearchRoomEntity room in hotelSearch.ListRooms)
            {
                childCount = 0;
                foreach (ChildEntity child in room.Listchildern.OrderBy(f => f.Age))
                {
                    if (child.Age >= 0 && child.Age <= 5)
                    {
                        childCount++;
                        if (childCount > room.AdultsPerRoom)
                        {
                            if (child.Age <= 2)
                            {
                                child.AccommodationString = "CRIB"; // TODO: Move to const
                            }
                            else
                            {
                                child.AccommodationString = "XBED"; // TODO: Move to const
                            }
                        }
                        else
                        {
                            child.AccommodationString = "CIPB";
                        }
                    }
                    else
                    {
                        child.AccommodationString = "XBED"; // TODO: Move to const
                    }
                }
            }

            roomCount = 0;
            foreach (HotelSearchRoomEntity room in hotelSearch.ListRooms)
            {
                roomCount++;
                childCount = 0;
                foreach (ChildEntity child in room.Listchildern)
                {
                    childCount++;
                    urlParams.AppendFormat(
                        CultureInfo.InvariantCulture,
                        "&CA{0}{1}={2}&CB{0}{1}={3}", // TODO: Take this from config/const
                        roomCount,
                        childCount,
                        child.Age,
                        child.AccommodationString);
                }
            }

            return urlParams.ToString();
        }

        /// <summary>
        /// Gets the profile ID.
        /// </summary>
        /// <param name="partnerApiKey">The partner API key.</param>
        /// <returns>Returns the profile Id used for commission handling</returns>
        private static string GetProfileId(string partnerApiKey)
        {
            Partner partner = new PartnerManager().GetPartnerDetails(partnerApiKey);
            return (partner != null) ? partner.PartnerProfileCode : string.Empty;
        }

        /// <summary>
        /// Gets the CMP id.
        /// </summary>
        /// <param name="partnerApiKey">The partner API key.</param>
        /// <param name="language">The language.</param>
        /// <returns>
        /// Returns the CmpId
        /// </returns>
        private static string GetCmpId(string partnerApiKey, string language)
        {
            Partner partner = new PartnerManager().GetPartnerDetails(partnerApiKey);
            return (partner != null) ? partner.TrackingCode.Trim(): string.Empty;
        }       
    }
}
