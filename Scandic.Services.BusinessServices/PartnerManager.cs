﻿// <copyright file="PartnerManager.cs" company="Scandic Hotels">
// Copyright © Scandic
// </copyright>

namespace Scandic.Services.BusinessServices
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;  
    using System.Threading.Tasks;
    using System.Xml.Linq;
    using Scandic.Services.BusinessEntity;
    using Scandic.Services.Framework;         
    using Scandic.Services.ServiceAgents;
    using Scandic.Services.ServiceAgents.Cms.CmsProxy;
    using Scandic.Services.ServiceAgents.CmsEntity;

    /// <summary>
    ///  PartnerManager to provide parter details
    /// </summary>    
    public class PartnerManager : EntityManagerBase
    {
        /// <summary>
        /// Lock object
        /// </summary>
        private static Object lockObject = new Object();

        /// <summary>
        /// Initializes a new instance of the <see cref="PartnerManager"/> class.
        /// </summary>      
        public PartnerManager(bool forceRefresh = false)
        {
            this.CacheKey = "PartnerManager";
            this.Language = "en";
            this.TimeStampFile = Path.Combine(ConfigHelper.GetValue(ConfigKeys.AppPath),ConfigHelper.GetValue(ConfigKeys.TimeStampFileFrequent));
            this.Initialize();
        }

        /// <summary>
        /// Gets the Partners list
        /// </summary>       
        public List<Partner> Partners { get; private set; }

        /// <summary>
        /// Gets the partner details.
        /// </summary>
        /// <param name="apiKey">The API key.</param>
        /// <returns> returns partner details </returns>       
        public Partner GetPartnerDetails(string apiKey)
        {
            return this.Partners.Find(delegate(Partner p)
            {
                    //Fix for artf1283026
                return p.ApiKey.Equals(apiKey);
            });
        }

        /// <summary>
        /// Gets a value indicating whether [is partner kay valid] [the specified partner key].
        /// </summary>
        /// <param name="apiKey">The partner key.</param>
        /// <returns><c>true</c> if [is partner kay valid] [the specified partner key]; otherwise, <c>false</c>.</returns>       
        public bool IsPartnerKayValid(string apiKey)
        {
            bool isPartnerKeyValid = false;
            bool isWithinValidDateRange = false;
            Partner partner = this.Partners.Find(delegate(Partner p)
            {
                return p.ApiKey.Equals(apiKey);
            });

            if (partner != null)
            {
                isPartnerKeyValid = partner.ApiKey.Equals(apiKey);
                int startDate = partner.ValidityStartDate.CompareTo(DateTime.Today);
                int endDate = partner.ValidityEndDate.CompareTo(DateTime.Today);
                isWithinValidDateRange = startDate <= 0 && endDate >= 0;
            }

            return isPartnerKeyValid && isWithinValidDateRange;
        }

        /// <summary>
        /// Fills the partner information.
        /// </summary>        
        public override void SetManagedEntity()
        {   
            bool refreshEntity = !(this.IsCacheValid);

            if (!refreshEntity)
            {
                this.Partners = (List<Partner>)this.CachedValue;
            }

             refreshEntity = refreshEntity || this.Partners == null;

            // If an entity needs to be refreshed i.e.
            // it is not in cache but needs to be picked
            // from the offline file.
            if (refreshEntity)
            {
                // There can be more than one thread trying to refresh
                // So, allow only one thread to pick data from file.
                lock (lockObject)
                {
                    if (this.CachedValue == null)
                    {
                        var entity = this.RefreshEntity<List<Partner>>();
                        if (!(entity == default(List<Partner>)))
                        {
                            this.Partners = entity;
                        }
                        if (this.IsCached)
                        {
                            this.CachedValue = this.Partners;
                        }
                    }
                    else
                    {
                        this.Partners = (List<Partner>)this.CachedValue;
                        LogHelper.LogInfo("Partner already loaded by another thread", LogCategory.Content);
                    }
                }
            }
        }

        /// <summary>
        /// Gets the partner list from CMS.
        /// </summary>  
        public void DownloadPartners()
        {
            this.Partners = new List<Partner>();
            string partnerPageTypeID = ConfigHelper.GetValue(ConfigKeys.PartnerPageTypeID);
            int partnerContainerPageReferenceID = Convert.ToInt32(ConfigHelper.GetValue(ConfigKeys.PartnerContainerId));
            new RateTypeManager(this.Language).CachedValue = null;
            RateTypes rateTypes = new RateTypeManager(this.Language).RateTypes;       

            // fetch all Partner Categories Configured in CMS
            RawPage[] partnerCategoryPages = ContentAccess.GetPagesWithPageTypeID(partnerPageTypeID, partnerContainerPageReferenceID, "en");
            foreach (RawPage rawPage in partnerCategoryPages)
            {
                Partner partnerCategory = new Partner();
                if (ContentHelper.IsPagePublished(rawPage))
                {
                    string partnerName = string.Empty;
                    partnerName = ContentHelper.GetPropertyValue(rawPage, "PageName");
                    partnerName = partnerName.Replace(" ", string.Empty).Trim();
                    partnerCategory.Name = partnerName;
                    partnerCategory.ApiKey = ContentHelper.GetPropertyValue(rawPage, "APIKey");
                    string interfaceTemp = ContentHelper.GetPropertyValue(rawPage, "InterfaceList");
                    
                    List<InterfaceDetails> interfaceList = new List<InterfaceDetails>();
                    if (!string.IsNullOrEmpty(interfaceTemp))
                    {
                        foreach (string temp in interfaceTemp.Split(','))
                        {
                            InterfaceDetails interfaceDetails = new InterfaceDetails();
                            interfaceDetails.Name = temp;                            
                            partnerCategory.InterfaceList.Add(interfaceDetails);
                        }
                    }
                                  
                    string arbTemp = ContentHelper.GetPropertyValue(rawPage, "ARBCodes");
                    if (!string.IsNullOrEmpty(arbTemp))
                    {
                        foreach (string temp in arbTemp.Split(','))
                        {
                          string arbCode = string.Empty;
                          arbCode = temp.Replace("{", string.Empty);
                          arbCode = arbCode.Replace("}", string.Empty);
                            // if a rateCategory specific to bookingCode is returned, it indicates that the booking code is published
                            RateCategory blockCode = rateTypes.RateCategoryCollection.Find(delegate(RateCategory r)
                            {
                                return r.RateCategoryId.Equals(arbCode);
                            });

                            if (blockCode != null)
                            {
                                partnerCategory.ArbCodes.Add(arbCode);
                                partnerCategory.ArbCodesMapWithOperaId.Add(blockCode.OperaId, arbCode);
                            }
                        }
                    }
                
                    if (!string.IsNullOrEmpty(ContentHelper.GetPropertyValue(rawPage, "IsPartnerEnabled")))
                    {
                        partnerCategory.IsPartnerEnabled = Convert.ToBoolean(ContentHelper.GetPropertyValue(rawPage, "IsPartnerEnabled"));
                    }

                    partnerCategory.PartnerProfileCode = ContentHelper.GetPropertyValue(rawPage, "PartnerProfileCode");

                    string promocodeTemp = ContentHelper.GetPropertyValue(rawPage, "PromoCodes");
                    if (!string.IsNullOrEmpty(promocodeTemp))
                    {
                        foreach (string temp in promocodeTemp.Split(','))
                        {
                            string promocodeList = string.Empty;
                            promocodeList = temp.Trim();
                            partnerCategory.PromoCodes.Add(promocodeList);
                        }
                    }

                    string tempTrackingCode = string.Empty;
                    tempTrackingCode = ContentHelper.GetPropertyValue(rawPage, "TrackingCode");
                    if (!string.IsNullOrEmpty(tempTrackingCode))
                    {
                        tempTrackingCode = tempTrackingCode.Replace(" ", string.Empty).Trim();
                        partnerCategory.TrackingCode = tempTrackingCode;
                    }
                  
                    string date = ContentHelper.GetPropertyValue(rawPage, "ValidityEndDate");
                    if (!string.IsNullOrEmpty(date))
                    {
                        partnerCategory.ValidityEndDate = Convert.ToDateTime(date);
                    }
                    else
                    {
                        partnerCategory.ValidityEndDate = DateTime.MaxValue;
                    }

                    date = ContentHelper.GetPropertyValue(rawPage, "ValidityStartDate");
                    if (!string.IsNullOrEmpty(date))
                    {
                        partnerCategory.ValidityStartDate = Convert.ToDateTime(date);
                    }

                    this.Partners.Add(partnerCategory);
                }
            }

            Serializer.ObjectToFile(this.OfflineFile, this.Partners);
        }
    }
}
